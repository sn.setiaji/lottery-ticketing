import Muc from 'components/odometer'

$('.l-modal').hide();

$(function() {

    $('#btn-lottery').on('click', function(e) {
        var min = 0;   //minimum nuber can reach
        var max = 1090 + 1; //maximum number can reach on lettery
        var arrayList = [];
        
        var random = Math.floor(Math.random() * (+max - +min)) + +min;
        arrayList.push(random)
        
        $('.number-winner').html(random);

        setTimeout(function(){
            $('.l-modal').show()
        }, 1800)

        setTimeout(function(){
            var el = document.querySelector('.odometer');

            var od = new Odometer({
                el: el
            });
            
            el.innerHTML = random;
        }, 1000);
    });

    $('.close-modal').click(function(){
        $('.l-modal').hide();
    })
    
    $('#close-modal').click(function(){
        $('.l-modal').hide();
    })
  });