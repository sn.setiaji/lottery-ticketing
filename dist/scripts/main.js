(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"E:\\Doco-Learn\\_Code\\_Form-Wizzard\\src\\src\\scripts\\components\\odometer.js":[function(require,module,exports){
'use strict';

var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
};

(function () {
  var COUNT_FRAMERATE,
      COUNT_MS_PER_FRAME,
      DIGIT_FORMAT,
      DIGIT_HTML,
      DIGIT_SPEEDBOOST,
      DURATION,
      FORMAT_MARK_HTML,
      FORMAT_PARSER,
      FRAMERATE,
      FRAMES_PER_VALUE,
      MS_PER_FRAME,
      MutationObserver,
      Odometer,
      RIBBON_HTML,
      TRANSITION_END_EVENTS,
      TRANSITION_SUPPORT,
      VALUE_HTML,
      addClass,
      createFromHTML,
      fractionalPart,
      now,
      removeClass,
      requestAnimationFrame,
      round,
      transitionCheckStyles,
      trigger,
      truncate,
      wrapJQuery,
      _jQueryWrapped,
      _old,
      _ref,
      _ref1,
      __slice = [].slice;

  VALUE_HTML = '<span class="odometer-value"></span>';

  RIBBON_HTML = '<span class="odometer-ribbon"><span class="odometer-ribbon-inner">' + VALUE_HTML + '</span></span>';

  DIGIT_HTML = '<span class="odometer-digit"><span class="odometer-digit-spacer">8</span><span class="odometer-digit-inner">' + RIBBON_HTML + '</span></span>';

  FORMAT_MARK_HTML = '<span class="odometer-formatting-mark"></span>';

  DIGIT_FORMAT = '(,ddd).dd';

  FORMAT_PARSER = /^\(?([^)]*)\)?(?:(.)(d+))?$/;

  FRAMERATE = 30;

  DURATION = 2000;

  COUNT_FRAMERATE = 20;

  FRAMES_PER_VALUE = 2;

  DIGIT_SPEEDBOOST = .5;

  MS_PER_FRAME = 1000 / FRAMERATE;

  COUNT_MS_PER_FRAME = 1000 / COUNT_FRAMERATE;

  TRANSITION_END_EVENTS = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd';

  transitionCheckStyles = document.createElement('div').style;

  TRANSITION_SUPPORT = transitionCheckStyles.transition != null || transitionCheckStyles.webkitTransition != null || transitionCheckStyles.mozTransition != null || transitionCheckStyles.oTransition != null;

  requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;

  createFromHTML = function createFromHTML(html) {
    var el;
    el = document.createElement('div');
    el.innerHTML = html;
    return el.children[0];
  };

  removeClass = function removeClass(el, name) {
    return el.className = el.className.replace(new RegExp("(^| )" + name.split(' ').join('|') + "( |$)", 'gi'), ' ');
  };

  addClass = function addClass(el, name) {
    removeClass(el, name);
    return el.className += " " + name;
  };

  trigger = function trigger(el, name) {
    var evt;
    if (document.createEvent != null) {
      evt = document.createEvent('HTMLEvents');
      evt.initEvent(name, true, true);
      return el.dispatchEvent(evt);
    }
  };

  now = function now() {
    var _ref, _ref1;
    return (_ref = (_ref1 = window.performance) != null ? typeof _ref1.now === "function" ? _ref1.now() : void 0 : void 0) != null ? _ref : +new Date();
  };

  round = function round(val, precision) {
    if (precision == null) {
      precision = 0;
    }
    if (!precision) {
      return Math.round(val);
    }
    val *= Math.pow(10, precision);
    val += 0.5;
    val = Math.floor(val);
    return val /= Math.pow(10, precision);
  };

  truncate = function truncate(val) {
    if (val < 0) {
      return Math.ceil(val);
    } else {
      return Math.floor(val);
    }
  };

  fractionalPart = function fractionalPart(val) {
    return val - round(val);
  };

  _jQueryWrapped = false;

  (wrapJQuery = function wrapJQuery() {
    var property, _i, _len, _ref, _results;
    if (_jQueryWrapped) {
      return;
    }
    if (window.jQuery != null) {
      _jQueryWrapped = true;
      _ref = ['html', 'text'];
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        property = _ref[_i];
        _results.push(function (property) {
          var old;
          old = window.jQuery.fn[property];
          return window.jQuery.fn[property] = function (val) {
            var _ref1;
            if (val == null || ((_ref1 = this[0]) != null ? _ref1.odometer : void 0) == null) {
              return old.apply(this, arguments);
            }
            return this[0].odometer.update(val);
          };
        }(property));
      }
      return _results;
    }
  })();

  setTimeout(wrapJQuery, 0);

  Odometer = function () {
    function Odometer(options) {
      var e,
          k,
          property,
          v,
          _base,
          _i,
          _len,
          _ref,
          _ref1,
          _ref2,
          _this = this;
      this.options = options;
      this.el = this.options.el;
      if (this.el.odometer != null) {
        return this.el.odometer;
      }
      this.el.odometer = this;
      _ref = Odometer.options;
      for (k in _ref) {
        v = _ref[k];
        if (this.options[k] == null) {
          this.options[k] = v;
        }
      }
      if ((_base = this.options).duration == null) {
        _base.duration = DURATION;
      }
      this.MAX_VALUES = this.options.duration / MS_PER_FRAME / FRAMES_PER_VALUE | 0;
      this.resetFormat();
      this.value = this.cleanValue((_ref1 = this.options.value) != null ? _ref1 : '');
      this.renderInside();
      this.render();
      try {
        _ref2 = ['innerHTML', 'innerText', 'textContent'];
        for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
          property = _ref2[_i];
          if (this.el[property] != null) {
            (function (property) {
              return Object.defineProperty(_this.el, property, {
                get: function get() {
                  var _ref3;
                  if (property === 'innerHTML') {
                    return _this.inside.outerHTML;
                  } else {
                    return (_ref3 = _this.inside.innerText) != null ? _ref3 : _this.inside.textContent;
                  }
                },
                set: function set(val) {
                  return _this.update(val);
                }
              });
            })(property);
          }
        }
      } catch (_error) {
        e = _error;
        this.watchForMutations();
      }
      this;
    }

    Odometer.prototype.renderInside = function () {
      this.inside = document.createElement('div');
      this.inside.className = 'odometer-inside';
      this.el.innerHTML = '';
      return this.el.appendChild(this.inside);
    };

    Odometer.prototype.watchForMutations = function () {
      var e,
          _this = this;
      if (MutationObserver == null) {
        return;
      }
      try {
        if (this.observer == null) {
          this.observer = new MutationObserver(function (mutations) {
            var newVal;
            newVal = _this.el.innerText;
            _this.renderInside();
            _this.render(_this.value);
            return _this.update(newVal);
          });
        }
        this.watchMutations = true;
        return this.startWatchingMutations();
      } catch (_error) {
        e = _error;
      }
    };

    Odometer.prototype.startWatchingMutations = function () {
      if (this.watchMutations) {
        return this.observer.observe(this.el, {
          childList: true
        });
      }
    };

    Odometer.prototype.stopWatchingMutations = function () {
      var _ref;
      return (_ref = this.observer) != null ? _ref.disconnect() : void 0;
    };

    Odometer.prototype.cleanValue = function (val) {
      var _ref;
      if (typeof val === 'string') {
        val = val.replace((_ref = this.format.radix) != null ? _ref : '.', '<radix>');
        val = val.replace(/[.,]/g, '');
        val = val.replace('<radix>', '.');
        val = parseFloat(val, 10) || 0;
      }
      return round(val, this.format.precision);
    };

    Odometer.prototype.bindTransitionEnd = function () {
      var event,
          renderEnqueued,
          _i,
          _len,
          _ref,
          _results,
          _this = this;
      if (this.transitionEndBound) {
        return;
      }
      this.transitionEndBound = true;
      renderEnqueued = false;
      _ref = TRANSITION_END_EVENTS.split(' ');
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        event = _ref[_i];
        _results.push(this.el.addEventListener(event, function () {
          if (renderEnqueued) {
            return true;
          }
          renderEnqueued = true;
          setTimeout(function () {
            _this.render();
            renderEnqueued = false;
            return trigger(_this.el, 'odometerdone');
          }, 0);
          return true;
        }, false));
      }
      return _results;
    };

    Odometer.prototype.resetFormat = function () {
      var format, fractional, parsed, precision, radix, repeating, _ref, _ref1;
      format = (_ref = this.options.format) != null ? _ref : DIGIT_FORMAT;
      format || (format = 'd');
      parsed = FORMAT_PARSER.exec(format);
      if (!parsed) {
        throw new Error("Odometer: Unparsable digit format");
      }
      _ref1 = parsed.slice(1, 4), repeating = _ref1[0], radix = _ref1[1], fractional = _ref1[2];
      precision = (fractional != null ? fractional.length : void 0) || 0;
      return this.format = {
        repeating: repeating,
        radix: radix,
        precision: precision
      };
    };

    Odometer.prototype.render = function (value) {
      var classes, cls, digit, match, newClasses, theme, wholePart, _i, _j, _len, _len1, _ref;
      if (value == null) {
        value = this.value;
      }
      this.stopWatchingMutations();
      this.resetFormat();
      this.inside.innerHTML = '';
      theme = this.options.theme;
      classes = this.el.className.split(' ');
      newClasses = [];
      for (_i = 0, _len = classes.length; _i < _len; _i++) {
        cls = classes[_i];
        if (!cls.length) {
          continue;
        }
        if (match = /^odometer-theme-(.+)$/.exec(cls)) {
          theme = match[1];
          continue;
        }
        if (/^odometer(-|$)/.test(cls)) {
          continue;
        }
        newClasses.push(cls);
      }
      newClasses.push('odometer');
      if (!TRANSITION_SUPPORT) {
        newClasses.push('odometer-no-transitions');
      }
      if (theme) {
        newClasses.push("odometer-theme-" + theme);
      } else {
        newClasses.push("odometer-auto-theme");
      }
      this.el.className = newClasses.join(' ');
      this.ribbons = {};
      this.digits = [];
      wholePart = !this.format.precision || !fractionalPart(value) || false;
      _ref = value.toString().split('').reverse();
      for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
        digit = _ref[_j];
        if (digit === '.') {
          wholePart = true;
        }
        this.addDigit(digit, wholePart);
      }
      return this.startWatchingMutations();
    };

    Odometer.prototype.update = function (newValue) {
      var diff,
          _this = this;
      newValue = this.cleanValue(newValue);
      if (!(diff = newValue - this.value)) {
        return;
      }
      removeClass(this.el, 'odometer-animating-up odometer-animating-down odometer-animating');
      if (diff > 0) {
        addClass(this.el, 'odometer-animating-up');
      } else {
        addClass(this.el, 'odometer-animating-down');
      }
      this.stopWatchingMutations();
      this.animate(newValue);
      this.startWatchingMutations();
      setTimeout(function () {
        _this.el.offsetHeight;
        return addClass(_this.el, 'odometer-animating');
      }, 0);
      return this.value = newValue;
    };

    Odometer.prototype.renderDigit = function () {
      return createFromHTML(DIGIT_HTML);
    };

    Odometer.prototype.insertDigit = function (digit, before) {
      if (before != null) {
        return this.inside.insertBefore(digit, before);
      } else if (!this.inside.children.length) {
        return this.inside.appendChild(digit);
      } else {
        return this.inside.insertBefore(digit, this.inside.children[0]);
      }
    };

    Odometer.prototype.addSpacer = function (chr, before, extraClasses) {
      var spacer;
      spacer = createFromHTML(FORMAT_MARK_HTML);
      spacer.innerHTML = chr;
      if (extraClasses) {
        addClass(spacer, extraClasses);
      }
      return this.insertDigit(spacer, before);
    };

    Odometer.prototype.addDigit = function (value, repeating) {
      var chr, digit, resetted, _ref;
      if (repeating == null) {
        repeating = true;
      }
      if (value === '-') {
        return this.addSpacer(value, null, 'odometer-negation-mark');
      }
      if (value === '.') {
        return this.addSpacer((_ref = this.format.radix) != null ? _ref : '.', null, 'odometer-radix-mark');
      }
      if (repeating) {
        resetted = false;
        while (true) {
          if (!this.format.repeating.length) {
            if (resetted) {
              throw new Error("Bad odometer format without digits");
            }
            this.resetFormat();
            resetted = true;
          }
          chr = this.format.repeating[this.format.repeating.length - 1];
          this.format.repeating = this.format.repeating.substring(0, this.format.repeating.length - 1);
          if (chr === 'd') {
            break;
          }
          this.addSpacer(chr);
        }
      }
      digit = this.renderDigit();
      digit.querySelector('.odometer-value').innerHTML = value;
      this.digits.push(digit);
      return this.insertDigit(digit);
    };

    Odometer.prototype.animate = function (newValue) {
      if (!TRANSITION_SUPPORT || this.options.animation === 'count') {
        return this.animateCount(newValue);
      } else {
        return this.animateSlide(newValue);
      }
    };

    Odometer.prototype.animateCount = function (newValue) {
      var cur,
          diff,
          last,
          start,
          _tick,
          _this = this;
      if (!(diff = +newValue - this.value)) {
        return;
      }
      start = last = now();
      cur = this.value;
      return (_tick = function tick() {
        var delta, dist, fraction;
        if (now() - start > _this.options.duration) {
          _this.value = newValue;
          _this.render();
          trigger(_this.el, 'odometerdone');
          return;
        }
        delta = now() - last;
        if (delta > COUNT_MS_PER_FRAME) {
          last = now();
          fraction = delta / _this.options.duration;
          dist = diff * fraction;
          cur += dist;
          _this.render(Math.round(cur));
        }
        if (requestAnimationFrame != null) {
          return requestAnimationFrame(_tick);
        } else {
          return setTimeout(_tick, COUNT_MS_PER_FRAME);
        }
      })();
    };

    Odometer.prototype.getDigitCount = function () {
      var i, max, value, values, _i, _len;
      values = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      for (i = _i = 0, _len = values.length; _i < _len; i = ++_i) {
        value = values[i];
        values[i] = Math.abs(value);
      }
      max = Math.max.apply(Math, values);
      return Math.ceil(Math.log(max + 1) / Math.log(10));
    };

    Odometer.prototype.getFractionalDigitCount = function () {
      var i, parser, parts, value, values, _i, _len;
      values = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      parser = /^\-?\d*\.(\d*?)0*$/;
      for (i = _i = 0, _len = values.length; _i < _len; i = ++_i) {
        value = values[i];
        values[i] = value.toString();
        parts = parser.exec(values[i]);
        if (parts == null) {
          values[i] = 0;
        } else {
          values[i] = parts[1].length;
        }
      }
      return Math.max.apply(Math, values);
    };

    Odometer.prototype.resetDigits = function () {
      this.digits = [];
      this.ribbons = [];
      this.inside.innerHTML = '';
      return this.resetFormat();
    };

    Odometer.prototype.animateSlide = function (newValue) {
      var boosted, cur, diff, digitCount, digits, dist, end, fractionalCount, frame, frames, i, incr, j, mark, numEl, oldValue, start, _base, _i, _j, _k, _l, _len, _len1, _len2, _m, _ref, _results;
      oldValue = this.value;
      fractionalCount = this.getFractionalDigitCount(oldValue, newValue);
      if (fractionalCount) {
        newValue = newValue * Math.pow(10, fractionalCount);
        oldValue = oldValue * Math.pow(10, fractionalCount);
      }
      if (!(diff = newValue - oldValue)) {
        return;
      }
      this.bindTransitionEnd();
      digitCount = this.getDigitCount(oldValue, newValue);
      digits = [];
      boosted = 0;
      for (i = _i = 0; 0 <= digitCount ? _i < digitCount : _i > digitCount; i = 0 <= digitCount ? ++_i : --_i) {
        start = truncate(oldValue / Math.pow(10, digitCount - i - 1));
        end = truncate(newValue / Math.pow(10, digitCount - i - 1));
        dist = end - start;
        if (Math.abs(dist) > this.MAX_VALUES) {
          frames = [];
          incr = dist / (this.MAX_VALUES + this.MAX_VALUES * boosted * DIGIT_SPEEDBOOST);
          cur = start;
          while (dist > 0 && cur < end || dist < 0 && cur > end) {
            frames.push(Math.round(cur));
            cur += incr;
          }
          if (frames[frames.length - 1] !== end) {
            frames.push(end);
          }
          boosted++;
        } else {
          frames = function () {
            _results = [];
            for (var _j = start; start <= end ? _j <= end : _j >= end; start <= end ? _j++ : _j--) {
              _results.push(_j);
            }
            return _results;
          }.apply(this);
        }
        for (i = _k = 0, _len = frames.length; _k < _len; i = ++_k) {
          frame = frames[i];
          frames[i] = Math.abs(frame % 10);
        }
        digits.push(frames);
      }
      this.resetDigits();
      _ref = digits.reverse();
      for (i = _l = 0, _len1 = _ref.length; _l < _len1; i = ++_l) {
        frames = _ref[i];
        if (!this.digits[i]) {
          this.addDigit(' ', i >= fractionalCount);
        }
        if ((_base = this.ribbons)[i] == null) {
          _base[i] = this.digits[i].querySelector('.odometer-ribbon-inner');
        }
        this.ribbons[i].innerHTML = '';
        if (diff < 0) {
          frames = frames.reverse();
        }
        for (j = _m = 0, _len2 = frames.length; _m < _len2; j = ++_m) {
          frame = frames[j];
          numEl = document.createElement('div');
          numEl.className = 'odometer-value';
          numEl.innerHTML = frame;
          this.ribbons[i].appendChild(numEl);
          if (j === frames.length - 1) {
            addClass(numEl, 'odometer-last-value');
          }
          if (j === 0) {
            addClass(numEl, 'odometer-first-value');
          }
        }
      }
      if (start < 0) {
        this.addDigit('-');
      }
      mark = this.inside.querySelector('.odometer-radix-mark');
      if (mark != null) {
        mark.parent.removeChild(mark);
      }
      if (fractionalCount) {
        return this.addSpacer(this.format.radix, this.digits[fractionalCount - 1], 'odometer-radix-mark');
      }
    };

    return Odometer;
  }();

  Odometer.options = (_ref = window.odometerOptions) != null ? _ref : {};

  setTimeout(function () {
    var k, v, _base, _ref1, _results;
    if (window.odometerOptions) {
      _ref1 = window.odometerOptions;
      _results = [];
      for (k in _ref1) {
        v = _ref1[k];
        _results.push((_base = Odometer.options)[k] != null ? (_base = Odometer.options)[k] : _base[k] = v);
      }
      return _results;
    }
  }, 0);

  Odometer.init = function () {
    var el, elements, _i, _len, _ref1, _results;
    if (document.querySelectorAll == null) {
      return;
    }
    elements = document.querySelectorAll(Odometer.options.selector || '.odometer');
    _results = [];
    for (_i = 0, _len = elements.length; _i < _len; _i++) {
      el = elements[_i];
      _results.push(el.odometer = new Odometer({
        el: el,
        value: (_ref1 = el.innerText) != null ? _ref1 : el.textContent
      }));
    }
    return _results;
  };

  if (((_ref1 = document.documentElement) != null ? _ref1.doScroll : void 0) != null && document.createEventObject != null) {
    _old = document.onreadystatechange;
    document.onreadystatechange = function () {
      if (document.readyState === 'complete' && Odometer.options.auto !== false) {
        Odometer.init();
      }
      return _old != null ? _old.apply(this, arguments) : void 0;
    };
  } else {
    document.addEventListener('DOMContentLoaded', function () {
      if (Odometer.options.auto !== false) {
        return Odometer.init();
      }
    }, false);
  }

  if (typeof define === 'function' && define.amd) {
    define(['jquery'], function () {
      return Odometer;
    });
  } else if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === !'undefined') {
    module.exports = Odometer;
  } else {
    window.Odometer = Odometer;
  }
}).call(undefined);


},{}],"E:\\Doco-Learn\\_Code\\_Form-Wizzard\\src\\src\\scripts\\main.js":[function(require,module,exports){
'use strict';

var _odometer = require('./components/odometer');

var _odometer2 = _interopRequireDefault(_odometer);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

$('.l-modal').hide();

$(function () {

    $('#btn-lottery').on('click', function (e) {
        var min = 0; //minimum nuber can reach
        var max = 1090 + 1; //maximum number can reach on lettery
        var arrayList = [];

        var random = Math.floor(Math.random() * (+max - +min)) + +min;
        arrayList.push(random);

        $('.number-winner').html(random);

        setTimeout(function () {
            $('.l-modal').show();
        }, 1800);

        setTimeout(function () {
            var el = document.querySelector('.odometer');

            var od = new Odometer({
                el: el
            });

            el.innerHTML = random;
        }, 1000);
    });

    $('.close-modal').click(function () {
        $('.l-modal').hide();
    });

    $('#close-modal').click(function () {
        $('.l-modal').hide();
    });
});

},{"./components/odometer":"E:\\Doco-Learn\\_Code\\_Form-Wizzard\\src\\src\\scripts\\components\\odometer.js"}]},{},["E:\\Doco-Learn\\_Code\\_Form-Wizzard\\src\\src\\scripts\\main.js"])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvc2NyaXB0cy9jb21wb25lbnRzL29kb21ldGVyLmpzIiwic3JjL3NjcmlwdHMvbWFpbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7QUNBQSxDQUFDLFlBQVc7RUFDVixJQUFBLGVBQUE7TUFBQSxrQkFBQTtNQUFBLFlBQUE7TUFBQSxVQUFBO01BQUEsZ0JBQUE7TUFBQSxRQUFBO01BQUEsZ0JBQUE7TUFBQSxhQUFBO01BQUEsU0FBQTtNQUFBLGdCQUFBO01BQUEsWUFBQTtNQUFBLGdCQUFBO01BQUEsUUFBQTtNQUFBLFdBQUE7TUFBQSxxQkFBQTtNQUFBLGtCQUFBO01BQUEsVUFBQTtNQUFBLFFBQUE7TUFBQSxjQUFBO01BQUEsY0FBQTtNQUFBLEdBQUE7TUFBQSxXQUFBO01BQUEscUJBQUE7TUFBQSxLQUFBO01BQUEscUJBQUE7TUFBQSxPQUFBO01BQUEsUUFBQTtNQUFBLFVBQUE7TUFBQSxjQUFBO01BQUEsSUFBQTtNQUFBLElBQUE7TUFBQSxLQUFBO01BQ0UsT0FBQSxHQUFVLEVBQUEsQ0FEWixLQUFBLENBQUE7O0VBR0EsVUFBQSxHQUFBLHNDQUFBLENBQUE7O0VBRUEsV0FBQSxHQUFjLG9FQUFBLEdBQUEsVUFBQSxHQUFkLGdCQUFBLENBQUE7O0VBRUEsVUFBQSxHQUFhLDhHQUFBLEdBQUEsV0FBQSxHQUFiLGdCQUFBLENBQUE7O0VBRUEsZ0JBQUEsR0FBQSxnREFBQSxDQUFBOztFQUVBLFlBQUEsR0FBQSxXQUFBLENBQUE7O0VBRUEsYUFBQSxHQUFBLDZCQUFBLENBQUE7O0VBRUEsU0FBQSxHQUFBLEVBQUEsQ0FBQTs7RUFFQSxRQUFBLEdBQUEsSUFBQSxDQUFBOztFQUVBLGVBQUEsR0FBQSxFQUFBLENBQUE7O0VBRUEsZ0JBQUEsR0FBQSxDQUFBLENBQUE7O0VBRUEsZ0JBQUEsR0FBQSxFQUFBLENBQUE7O0VBRUEsWUFBQSxHQUFlLElBQUEsR0FBZixTQUFBLENBQUE7O0VBRUEsa0JBQUEsR0FBcUIsSUFBQSxHQUFyQixlQUFBLENBQUE7O0VBRUEscUJBQUEsR0FBQSxpRkFBQSxDQUFBOztFQUVBLHFCQUFBLEdBQXdCLFFBQUEsQ0FBQSxhQUFBLENBQUEsS0FBQSxDQUFBLENBQXhCLEtBQUEsQ0FBQTs7RUFFQSxrQkFBQSxHQUFzQixxQkFBQSxDQUFBLFVBQUEsSUFBRCxJQUFDLElBQThDLHFCQUFBLENBQUEsZ0JBQUEsSUFBL0MsSUFBQyxJQUFrRyxxQkFBQSxDQUFBLGFBQUEsSUFBbkcsSUFBQyxJQUFtSixxQkFBQSxDQUFBLFdBQUEsSUFBekssSUFBQSxDQUFBOztFQUVBLHFCQUFBLEdBQXdCLE1BQUEsQ0FBQSxxQkFBQSxJQUFnQyxNQUFBLENBQWhDLHdCQUFBLElBQW1FLE1BQUEsQ0FBbkUsMkJBQUEsSUFBeUcsTUFBQSxDQUFqSSx1QkFBQSxDQUFBOztFQUVBLGdCQUFBLEdBQW1CLE1BQUEsQ0FBQSxnQkFBQSxJQUEyQixNQUFBLENBQTNCLHNCQUFBLElBQTRELE1BQUEsQ0FBL0UsbUJBQUEsQ0FBQTs7RUFFQSxjQUFBLEdBQWlCLFNBQUEsY0FBQSxDQUFBLElBQUEsRUFBZTtJQUM5QixJQUFBLEVBQUEsQ0FBQTtJQUNBLEVBQUEsR0FBSyxRQUFBLENBQUEsYUFBQSxDQUFMLEtBQUssQ0FBTCxDQUFBO0lBQ0EsRUFBQSxDQUFBLFNBQUEsR0FBQSxJQUFBLENBQUE7SUFDQSxPQUFPLEVBQUEsQ0FBQSxRQUFBLENBQVAsQ0FBTyxDQUFQLENBQUE7R0FKRixDQUFBOztFQU9BLFdBQUEsR0FBYyxTQUFBLFdBQUEsQ0FBQSxFQUFBLEVBQUEsSUFBQSxFQUFtQjtJQUMvQixPQUFPLEVBQUEsQ0FBQSxTQUFBLEdBQWUsRUFBQSxDQUFBLFNBQUEsQ0FBQSxPQUFBLENBQXFCLElBQUEsTUFBQSxDQUFXLE9BQUEsR0FBVyxJQUFBLENBQUEsS0FBQSxDQUFBLEdBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBWCxHQUFXLENBQVgsR0FBWCxPQUFBLEVBQXJCLElBQXFCLENBQXJCLEVBQXRCLEdBQXNCLENBQXRCLENBQUE7R0FERixDQUFBOztFQUlBLFFBQUEsR0FBVyxTQUFBLFFBQUEsQ0FBQSxFQUFBLEVBQUEsSUFBQSxFQUFtQjtJQUM1QixXQUFBLENBQUEsRUFBQSxFQUFBLElBQUEsQ0FBQSxDQUFBO0lBQ0EsT0FBTyxFQUFBLENBQUEsU0FBQSxJQUFnQixHQUFBLEdBQXZCLElBQUEsQ0FBQTtHQUZGLENBQUE7O0VBS0EsT0FBQSxHQUFVLFNBQUEsT0FBQSxDQUFBLEVBQUEsRUFBQSxJQUFBLEVBQW1CO0lBQzNCLElBQUEsR0FBQSxDQUFBO0lBQ0EsSUFBSSxRQUFBLENBQUEsV0FBQSxJQUFKLElBQUEsRUFBa0M7TUFDaEMsR0FBQSxHQUFNLFFBQUEsQ0FBQSxXQUFBLENBQU4sWUFBTSxDQUFOLENBQUE7TUFDQSxHQUFBLENBQUEsU0FBQSxDQUFBLElBQUEsRUFBQSxJQUFBLEVBQUEsSUFBQSxDQUFBLENBQUE7TUFDQSxPQUFPLEVBQUEsQ0FBQSxhQUFBLENBQVAsR0FBTyxDQUFQLENBQUE7S0FDRDtHQU5ILENBQUE7O0VBU0EsR0FBQSxHQUFNLFNBQUEsR0FBQSxHQUFXO0lBQ2YsSUFBQSxJQUFBLEVBQUEsS0FBQSxDQUFBO0lBQ0EsT0FBTyxDQUFDLElBQUEsR0FBTyxDQUFDLEtBQUEsR0FBUSxNQUFBLENBQVQsV0FBQSxLQUFBLElBQUEsR0FBdUMsT0FBTyxLQUFBLENBQVAsR0FBQSxLQUFBLFVBQUEsR0FBa0MsS0FBQSxDQUFsQyxHQUFrQyxFQUFsQyxHQUFnRCxLQUF2RixDQUFBLEdBQWdHLEtBQXhHLENBQUEsS0FBQSxJQUFBLEdBQUEsSUFBQSxHQUFpSSxDQUFFLElBQTFJLElBQTBJLEVBQTFJLENBQUE7R0FGRixDQUFBOztFQUtBLEtBQUEsR0FBUSxTQUFBLEtBQUEsQ0FBQSxHQUFBLEVBQUEsU0FBQSxFQUF5QjtJQUMvQixJQUFJLFNBQUEsSUFBSixJQUFBLEVBQXVCO01BQ3JCLFNBQUEsR0FBQSxDQUFBLENBQUE7S0FDRDtJQUNELElBQUksQ0FBSixTQUFBLEVBQWdCO01BQ2QsT0FBTyxJQUFBLENBQUEsS0FBQSxDQUFQLEdBQU8sQ0FBUCxDQUFBO0tBQ0Q7SUFDRCxHQUFBLElBQU8sSUFBQSxDQUFBLEdBQUEsQ0FBQSxFQUFBLEVBQVAsU0FBTyxDQUFQLENBQUE7SUFDQSxHQUFBLElBQUEsR0FBQSxDQUFBO0lBQ0EsR0FBQSxHQUFNLElBQUEsQ0FBQSxLQUFBLENBQU4sR0FBTSxDQUFOLENBQUE7SUFDQSxPQUFPLEdBQUEsSUFBTyxJQUFBLENBQUEsR0FBQSxDQUFBLEVBQUEsRUFBZCxTQUFjLENBQWQsQ0FBQTtHQVZGLENBQUE7O0VBYUEsUUFBQSxHQUFXLFNBQUEsUUFBQSxDQUFBLEdBQUEsRUFBYztJQUN2QixJQUFJLEdBQUEsR0FBSixDQUFBLEVBQWE7TUFDWCxPQUFPLElBQUEsQ0FBQSxJQUFBLENBQVAsR0FBTyxDQUFQLENBQUE7S0FERixNQUVPO01BQ0wsT0FBTyxJQUFBLENBQUEsS0FBQSxDQUFQLEdBQU8sQ0FBUCxDQUFBO0tBQ0Q7R0FMSCxDQUFBOztFQVFBLGNBQUEsR0FBaUIsU0FBQSxjQUFBLENBQUEsR0FBQSxFQUFjO0lBQzdCLE9BQU8sR0FBQSxHQUFNLEtBQUEsQ0FBYixHQUFhLENBQWIsQ0FBQTtHQURGLENBQUE7O0VBSUEsY0FBQSxHQUFBLEtBQUEsQ0FBQTs7RUFFQSxDQUFDLFVBQUEsR0FBYSxTQUFBLFVBQUEsR0FBVztJQUN2QixJQUFBLFFBQUEsRUFBQSxFQUFBLEVBQUEsSUFBQSxFQUFBLElBQUEsRUFBQSxRQUFBLENBQUE7SUFDQSxJQUFBLGNBQUEsRUFBb0I7TUFDbEIsT0FBQTtLQUNEO0lBQ0QsSUFBSSxNQUFBLENBQUEsTUFBQSxJQUFKLElBQUEsRUFBMkI7TUFDekIsY0FBQSxHQUFBLElBQUEsQ0FBQTtNQUNBLElBQUEsR0FBTyxDQUFBLE1BQUEsRUFBUCxNQUFPLENBQVAsQ0FBQTtNQUNBLFFBQUEsR0FBQSxFQUFBLENBQUE7TUFDQSxLQUFLLEVBQUEsR0FBQSxDQUFBLEVBQVEsSUFBQSxHQUFPLElBQUEsQ0FBcEIsTUFBQSxFQUFpQyxFQUFBLEdBQWpDLElBQUEsRUFBQSxFQUFBLEVBQUEsRUFBa0Q7UUFDaEQsUUFBQSxHQUFXLElBQUEsQ0FBWCxFQUFXLENBQVgsQ0FBQTtRQUNBLFFBQUEsQ0FBQSxJQUFBLENBQWUsVUFBQSxRQUFBLEVBQW1CO1VBQ2hDLElBQUEsR0FBQSxDQUFBO1VBQ0EsR0FBQSxHQUFNLE1BQUEsQ0FBQSxNQUFBLENBQUEsRUFBQSxDQUFOLFFBQU0sQ0FBTixDQUFBO1VBQ0EsT0FBTyxNQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsQ0FBQSxRQUFBLENBQUEsR0FBNkIsVUFBQSxHQUFBLEVBQWM7WUFDaEQsSUFBQSxLQUFBLENBQUE7WUFDQSxJQUFLLEdBQUEsSUFBRCxJQUFDLElBQWlCLENBQUMsQ0FBQyxLQUFBLEdBQVEsSUFBQSxDQUFULENBQVMsQ0FBVCxLQUFBLElBQUEsR0FBNEIsS0FBQSxDQUE1QixRQUFBLEdBQTZDLEtBQTlDLENBQUEsS0FBdEIsSUFBQSxFQUFzRjtjQUNwRixPQUFPLEdBQUEsQ0FBQSxLQUFBLENBQUEsSUFBQSxFQUFQLFNBQU8sQ0FBUCxDQUFBO2FBQ0Q7WUFDRCxPQUFPLElBQUEsQ0FBQSxDQUFBLENBQUEsQ0FBQSxRQUFBLENBQUEsTUFBQSxDQUFQLEdBQU8sQ0FBUCxDQUFBO1dBTEYsQ0FBQTtTQUhhLENBQWYsUUFBZSxDQUFmLENBQUEsQ0FBQTtPQVdEO01BQ0QsT0FBQSxRQUFBLENBQUE7S0FDRDtHQXhCSCxHQUFBLENBQUE7O0VBMkJBLFVBQUEsQ0FBQSxVQUFBLEVBQUEsQ0FBQSxDQUFBLENBQUE7O0VBRUEsUUFBQSxHQUFZLFlBQVc7SUFDckIsU0FBQSxRQUFBLENBQUEsT0FBQSxFQUEyQjtNQUN6QixJQUFBLENBQUE7VUFBQSxDQUFBO1VBQUEsUUFBQTtVQUFBLENBQUE7VUFBQSxLQUFBO1VBQUEsRUFBQTtVQUFBLElBQUE7VUFBQSxJQUFBO1VBQUEsS0FBQTtVQUFBLEtBQUE7VUFDRSxLQUFBLEdBREYsSUFBQSxDQUFBO01BRUEsSUFBQSxDQUFBLE9BQUEsR0FBQSxPQUFBLENBQUE7TUFDQSxJQUFBLENBQUEsRUFBQSxHQUFVLElBQUEsQ0FBQSxPQUFBLENBQVYsRUFBQSxDQUFBO01BQ0EsSUFBSSxJQUFBLENBQUEsRUFBQSxDQUFBLFFBQUEsSUFBSixJQUFBLEVBQThCO1FBQzVCLE9BQU8sSUFBQSxDQUFBLEVBQUEsQ0FBUCxRQUFBLENBQUE7T0FDRDtNQUNELElBQUEsQ0FBQSxFQUFBLENBQUEsUUFBQSxHQUFBLElBQUEsQ0FBQTtNQUNBLElBQUEsR0FBTyxRQUFBLENBQVAsT0FBQSxDQUFBO01BQ0EsS0FBQSxDQUFBLElBQUEsSUFBQSxFQUFnQjtRQUNkLENBQUEsR0FBSSxJQUFBLENBQUosQ0FBSSxDQUFKLENBQUE7UUFDQSxJQUFJLElBQUEsQ0FBQSxPQUFBLENBQUEsQ0FBQSxDQUFBLElBQUosSUFBQSxFQUE2QjtVQUMzQixJQUFBLENBQUEsT0FBQSxDQUFBLENBQUEsQ0FBQSxHQUFBLENBQUEsQ0FBQTtTQUNEO09BQ0Y7TUFDRCxJQUFJLENBQUMsS0FBQSxHQUFRLElBQUEsQ0FBVCxPQUFBLEVBQUEsUUFBQSxJQUFKLElBQUEsRUFBNkM7UUFDM0MsS0FBQSxDQUFBLFFBQUEsR0FBQSxRQUFBLENBQUE7T0FDRDtNQUNELElBQUEsQ0FBQSxVQUFBLEdBQW9CLElBQUEsQ0FBQSxPQUFBLENBQUEsUUFBQSxHQUFELFlBQUMsR0FBRixnQkFBRSxHQUFwQixDQUFBLENBQUE7TUFDQSxJQUFBLENBQUEsV0FBQSxFQUFBLENBQUE7TUFDQSxJQUFBLENBQUEsS0FBQSxHQUFhLElBQUEsQ0FBQSxVQUFBLENBQWdCLENBQUMsS0FBQSxHQUFRLElBQUEsQ0FBQSxPQUFBLENBQVQsS0FBQSxLQUFBLElBQUEsR0FBQSxLQUFBLEdBQTdCLEVBQWEsQ0FBYixDQUFBO01BQ0EsSUFBQSxDQUFBLFlBQUEsRUFBQSxDQUFBO01BQ0EsSUFBQSxDQUFBLE1BQUEsRUFBQSxDQUFBO01BQ0EsSUFBSTtRQUNGLEtBQUEsR0FBUSxDQUFBLFdBQUEsRUFBQSxXQUFBLEVBQVIsYUFBUSxDQUFSLENBQUE7UUFDQSxLQUFLLEVBQUEsR0FBQSxDQUFBLEVBQVEsSUFBQSxHQUFPLEtBQUEsQ0FBcEIsTUFBQSxFQUFrQyxFQUFBLEdBQWxDLElBQUEsRUFBQSxFQUFBLEVBQUEsRUFBbUQ7VUFDakQsUUFBQSxHQUFXLEtBQUEsQ0FBWCxFQUFXLENBQVgsQ0FBQTtVQUNBLElBQUksSUFBQSxDQUFBLEVBQUEsQ0FBQSxRQUFBLENBQUEsSUFBSixJQUFBLEVBQStCO1lBQzdCLENBQUMsVUFBQSxRQUFBLEVBQW1CO2NBQ2xCLE9BQU8sTUFBQSxDQUFBLGNBQUEsQ0FBc0IsS0FBQSxDQUF0QixFQUFBLEVBQUEsUUFBQSxFQUEwQztnQkFDL0MsR0FBQSxFQUFLLFNBQUEsR0FBQSxHQUFXO2tCQUNkLElBQUEsS0FBQSxDQUFBO2tCQUNBLElBQUksUUFBQSxLQUFKLFdBQUEsRUFBOEI7b0JBQzVCLE9BQU8sS0FBQSxDQUFBLE1BQUEsQ0FBUCxTQUFBLENBQUE7bUJBREYsTUFFTztvQkFDTCxPQUFPLENBQUMsS0FBQSxHQUFRLEtBQUEsQ0FBQSxNQUFBLENBQVQsU0FBQSxLQUFBLElBQUEsR0FBQSxLQUFBLEdBQW1ELEtBQUEsQ0FBQSxNQUFBLENBQTFELFdBQUEsQ0FBQTttQkFDRDtpQkFQNEM7Z0JBUy9DLEdBQUEsRUFBSyxTQUFBLEdBQUEsQ0FBQSxHQUFBLEVBQWM7a0JBQ2pCLE9BQU8sS0FBQSxDQUFBLE1BQUEsQ0FBUCxHQUFPLENBQVAsQ0FBQTtpQkFDRDtlQVhJLENBQVAsQ0FBQTthQURGLEVBQUEsUUFBQSxDQUFBLENBQUE7V0FlRDtTQUNGO09BckJILENBc0JFLE9BQUEsTUFBQSxFQUFlO1FBQ2YsQ0FBQSxHQUFBLE1BQUEsQ0FBQTtRQUNBLElBQUEsQ0FBQSxpQkFBQSxFQUFBLENBQUE7T0FDRDtNQUNELElBQUEsQ0FBQTtLQUNEOztJQUVELFFBQUEsQ0FBQSxTQUFBLENBQUEsWUFBQSxHQUFrQyxZQUFXO01BQzNDLElBQUEsQ0FBQSxNQUFBLEdBQWMsUUFBQSxDQUFBLGFBQUEsQ0FBZCxLQUFjLENBQWQsQ0FBQTtNQUNBLElBQUEsQ0FBQSxNQUFBLENBQUEsU0FBQSxHQUFBLGlCQUFBLENBQUE7TUFDQSxJQUFBLENBQUEsRUFBQSxDQUFBLFNBQUEsR0FBQSxFQUFBLENBQUE7TUFDQSxPQUFPLElBQUEsQ0FBQSxFQUFBLENBQUEsV0FBQSxDQUFvQixJQUFBLENBQTNCLE1BQU8sQ0FBUCxDQUFBO0tBSkYsQ0FBQTs7SUFPQSxRQUFBLENBQUEsU0FBQSxDQUFBLGlCQUFBLEdBQXVDLFlBQVc7TUFDaEQsSUFBQSxDQUFBO1VBQ0UsS0FBQSxHQURGLElBQUEsQ0FBQTtNQUVBLElBQUksZ0JBQUEsSUFBSixJQUFBLEVBQThCO1FBQzVCLE9BQUE7T0FDRDtNQUNELElBQUk7UUFDRixJQUFJLElBQUEsQ0FBQSxRQUFBLElBQUosSUFBQSxFQUEyQjtVQUN6QixJQUFBLENBQUEsUUFBQSxHQUFnQixJQUFBLGdCQUFBLENBQXFCLFVBQUEsU0FBQSxFQUFvQjtZQUN2RCxJQUFBLE1BQUEsQ0FBQTtZQUNBLE1BQUEsR0FBUyxLQUFBLENBQUEsRUFBQSxDQUFULFNBQUEsQ0FBQTtZQUNBLEtBQUEsQ0FBQSxZQUFBLEVBQUEsQ0FBQTtZQUNBLEtBQUEsQ0FBQSxNQUFBLENBQWEsS0FBQSxDQUFiLEtBQUEsQ0FBQSxDQUFBO1lBQ0EsT0FBTyxLQUFBLENBQUEsTUFBQSxDQUFQLE1BQU8sQ0FBUCxDQUFBO1dBTGMsQ0FBaEIsQ0FBQTtTQU9EO1FBQ0QsSUFBQSxDQUFBLGNBQUEsR0FBQSxJQUFBLENBQUE7UUFDQSxPQUFPLElBQUEsQ0FBUCxzQkFBTyxFQUFQLENBQUE7T0FYRixDQVlFLE9BQUEsTUFBQSxFQUFlO1FBQ2YsQ0FBQSxHQUFBLE1BQUEsQ0FBQTtPQUNEO0tBcEJILENBQUE7O0lBdUJBLFFBQUEsQ0FBQSxTQUFBLENBQUEsc0JBQUEsR0FBNEMsWUFBVztNQUNyRCxJQUFJLElBQUEsQ0FBSixjQUFBLEVBQXlCO1FBQ3ZCLE9BQU8sSUFBQSxDQUFBLFFBQUEsQ0FBQSxPQUFBLENBQXNCLElBQUEsQ0FBdEIsRUFBQSxFQUErQjtVQUNwQyxTQUFBLEVBQVcsSUFBQTtTQUROLENBQVAsQ0FBQTtPQUdEO0tBTEgsQ0FBQTs7SUFRQSxRQUFBLENBQUEsU0FBQSxDQUFBLHFCQUFBLEdBQTJDLFlBQVc7TUFDcEQsSUFBQSxJQUFBLENBQUE7TUFDQSxPQUFPLENBQUMsSUFBQSxHQUFPLElBQUEsQ0FBUixRQUFBLEtBQUEsSUFBQSxHQUFpQyxJQUFBLENBQWpDLFVBQWlDLEVBQWpDLEdBQXFELEtBQTVELENBQUEsQ0FBQTtLQUZGLENBQUE7O0lBS0EsUUFBQSxDQUFBLFNBQUEsQ0FBQSxVQUFBLEdBQWdDLFVBQUEsR0FBQSxFQUFjO01BQzVDLElBQUEsSUFBQSxDQUFBO01BQ0EsSUFBSSxPQUFBLEdBQUEsS0FBSixRQUFBLEVBQTZCO1FBQzNCLEdBQUEsR0FBTSxHQUFBLENBQUEsT0FBQSxDQUFZLENBQUMsSUFBQSxHQUFPLElBQUEsQ0FBQSxNQUFBLENBQVIsS0FBQSxLQUFBLElBQUEsR0FBQSxJQUFBLEdBQVosR0FBQSxFQUFOLFNBQU0sQ0FBTixDQUFBO1FBQ0EsR0FBQSxHQUFNLEdBQUEsQ0FBQSxPQUFBLENBQUEsT0FBQSxFQUFOLEVBQU0sQ0FBTixDQUFBO1FBQ0EsR0FBQSxHQUFNLEdBQUEsQ0FBQSxPQUFBLENBQUEsU0FBQSxFQUFOLEdBQU0sQ0FBTixDQUFBO1FBQ0EsR0FBQSxHQUFNLFVBQUEsQ0FBQSxHQUFBLEVBQUEsRUFBQSxDQUFBLElBQU4sQ0FBQSxDQUFBO09BQ0Q7TUFDRCxPQUFPLEtBQUEsQ0FBQSxHQUFBLEVBQVcsSUFBQSxDQUFBLE1BQUEsQ0FBbEIsU0FBTyxDQUFQLENBQUE7S0FSRixDQUFBOztJQVdBLFFBQUEsQ0FBQSxTQUFBLENBQUEsaUJBQUEsR0FBdUMsWUFBVztNQUNoRCxJQUFBLEtBQUE7VUFBQSxjQUFBO1VBQUEsRUFBQTtVQUFBLElBQUE7VUFBQSxJQUFBO1VBQUEsUUFBQTtVQUNFLEtBQUEsR0FERixJQUFBLENBQUE7TUFFQSxJQUFJLElBQUEsQ0FBSixrQkFBQSxFQUE2QjtRQUMzQixPQUFBO09BQ0Q7TUFDRCxJQUFBLENBQUEsa0JBQUEsR0FBQSxJQUFBLENBQUE7TUFDQSxjQUFBLEdBQUEsS0FBQSxDQUFBO01BQ0EsSUFBQSxHQUFPLHFCQUFBLENBQUEsS0FBQSxDQUFQLEdBQU8sQ0FBUCxDQUFBO01BQ0EsUUFBQSxHQUFBLEVBQUEsQ0FBQTtNQUNBLEtBQUssRUFBQSxHQUFBLENBQUEsRUFBUSxJQUFBLEdBQU8sSUFBQSxDQUFwQixNQUFBLEVBQWlDLEVBQUEsR0FBakMsSUFBQSxFQUFBLEVBQUEsRUFBQSxFQUFrRDtRQUNoRCxLQUFBLEdBQVEsSUFBQSxDQUFSLEVBQVEsQ0FBUixDQUFBO1FBQ0EsUUFBQSxDQUFBLElBQUEsQ0FBYyxJQUFBLENBQUEsRUFBQSxDQUFBLGdCQUFBLENBQUEsS0FBQSxFQUFnQyxZQUFXO1VBQ3ZELElBQUEsY0FBQSxFQUFvQjtZQUNsQixPQUFBLElBQUEsQ0FBQTtXQUNEO1VBQ0QsY0FBQSxHQUFBLElBQUEsQ0FBQTtVQUNBLFVBQUEsQ0FBVyxZQUFXO1lBQ3BCLEtBQUEsQ0FBQSxNQUFBLEVBQUEsQ0FBQTtZQUNBLGNBQUEsR0FBQSxLQUFBLENBQUE7WUFDQSxPQUFPLE9BQUEsQ0FBUSxLQUFBLENBQVIsRUFBQSxFQUFQLGNBQU8sQ0FBUCxDQUFBO1dBSEYsRUFBQSxDQUFBLENBQUEsQ0FBQTtVQUtBLE9BQUEsSUFBQSxDQUFBO1NBVlksRUFBZCxLQUFjLENBQWQsQ0FBQSxDQUFBO09BWUQ7TUFDRCxPQUFBLFFBQUEsQ0FBQTtLQXpCRixDQUFBOztJQTRCQSxRQUFBLENBQUEsU0FBQSxDQUFBLFdBQUEsR0FBaUMsWUFBVztNQUMxQyxJQUFBLE1BQUEsRUFBQSxVQUFBLEVBQUEsTUFBQSxFQUFBLFNBQUEsRUFBQSxLQUFBLEVBQUEsU0FBQSxFQUFBLElBQUEsRUFBQSxLQUFBLENBQUE7TUFDQSxNQUFBLEdBQVMsQ0FBQyxJQUFBLEdBQU8sSUFBQSxDQUFBLE9BQUEsQ0FBUixNQUFBLEtBQUEsSUFBQSxHQUFBLElBQUEsR0FBVCxZQUFBLENBQUE7TUFDQSxNQUFBLEtBQVcsTUFBQSxHQUFYLEdBQUEsQ0FBQSxDQUFBO01BQ0EsTUFBQSxHQUFTLGFBQUEsQ0FBQSxJQUFBLENBQVQsTUFBUyxDQUFULENBQUE7TUFDQSxJQUFJLENBQUosTUFBQSxFQUFhO1FBQ1gsTUFBTSxJQUFBLEtBQUEsQ0FBTixtQ0FBTSxDQUFOLENBQUE7T0FDRDtNQUNELEtBQUEsR0FBUSxNQUFBLENBQUEsS0FBQSxDQUFBLENBQUEsRUFBUixDQUFRLENBQVIsRUFBNEIsU0FBQSxHQUFZLEtBQUEsQ0FBeEMsQ0FBd0MsQ0FBeEMsRUFBa0QsS0FBQSxHQUFRLEtBQUEsQ0FBMUQsQ0FBMEQsQ0FBMUQsRUFBb0UsVUFBQSxHQUFhLEtBQUEsQ0FBakYsQ0FBaUYsQ0FBakYsQ0FBQTtNQUNBLFNBQUEsR0FBWSxDQUFDLFVBQUEsSUFBQSxJQUFBLEdBQXFCLFVBQUEsQ0FBckIsTUFBQSxHQUF5QyxLQUExQyxDQUFBLEtBQVosQ0FBQSxDQUFBO01BQ0EsT0FBTyxJQUFBLENBQUEsTUFBQSxHQUFjO1FBQ25CLFNBQUEsRUFEbUIsU0FBQTtRQUVuQixLQUFBLEVBRm1CLEtBQUE7UUFHbkIsU0FBQSxFQUFXLFNBQUE7T0FIYixDQUFBO0tBVkYsQ0FBQTs7SUFpQkEsUUFBQSxDQUFBLFNBQUEsQ0FBQSxNQUFBLEdBQTRCLFVBQUEsS0FBQSxFQUFnQjtNQUMxQyxJQUFBLE9BQUEsRUFBQSxHQUFBLEVBQUEsS0FBQSxFQUFBLEtBQUEsRUFBQSxVQUFBLEVBQUEsS0FBQSxFQUFBLFNBQUEsRUFBQSxFQUFBLEVBQUEsRUFBQSxFQUFBLElBQUEsRUFBQSxLQUFBLEVBQUEsSUFBQSxDQUFBO01BQ0EsSUFBSSxLQUFBLElBQUosSUFBQSxFQUFtQjtRQUNqQixLQUFBLEdBQVEsSUFBQSxDQUFSLEtBQUEsQ0FBQTtPQUNEO01BQ0QsSUFBQSxDQUFBLHFCQUFBLEVBQUEsQ0FBQTtNQUNBLElBQUEsQ0FBQSxXQUFBLEVBQUEsQ0FBQTtNQUNBLElBQUEsQ0FBQSxNQUFBLENBQUEsU0FBQSxHQUFBLEVBQUEsQ0FBQTtNQUNBLEtBQUEsR0FBUSxJQUFBLENBQUEsT0FBQSxDQUFSLEtBQUEsQ0FBQTtNQUNBLE9BQUEsR0FBVSxJQUFBLENBQUEsRUFBQSxDQUFBLFNBQUEsQ0FBQSxLQUFBLENBQVYsR0FBVSxDQUFWLENBQUE7TUFDQSxVQUFBLEdBQUEsRUFBQSxDQUFBO01BQ0EsS0FBSyxFQUFBLEdBQUEsQ0FBQSxFQUFRLElBQUEsR0FBTyxPQUFBLENBQXBCLE1BQUEsRUFBb0MsRUFBQSxHQUFwQyxJQUFBLEVBQUEsRUFBQSxFQUFBLEVBQXFEO1FBQ25ELEdBQUEsR0FBTSxPQUFBLENBQU4sRUFBTSxDQUFOLENBQUE7UUFDQSxJQUFJLENBQUMsR0FBQSxDQUFMLE1BQUEsRUFBaUI7VUFDZixTQUFBO1NBQ0Q7UUFDRCxJQUFJLEtBQUEsR0FBUSx1QkFBQSxDQUFBLElBQUEsQ0FBWixHQUFZLENBQVosRUFBK0M7VUFDN0MsS0FBQSxHQUFRLEtBQUEsQ0FBUixDQUFRLENBQVIsQ0FBQTtVQUNBLFNBQUE7U0FDRDtRQUNELElBQUksZ0JBQUEsQ0FBQSxJQUFBLENBQUosR0FBSSxDQUFKLEVBQWdDO1VBQzlCLFNBQUE7U0FDRDtRQUNELFVBQUEsQ0FBQSxJQUFBLENBQUEsR0FBQSxDQUFBLENBQUE7T0FDRDtNQUNELFVBQUEsQ0FBQSxJQUFBLENBQUEsVUFBQSxDQUFBLENBQUE7TUFDQSxJQUFJLENBQUosa0JBQUEsRUFBeUI7UUFDdkIsVUFBQSxDQUFBLElBQUEsQ0FBQSx5QkFBQSxDQUFBLENBQUE7T0FDRDtNQUNELElBQUEsS0FBQSxFQUFXO1FBQ1QsVUFBQSxDQUFBLElBQUEsQ0FBZ0IsaUJBQUEsR0FBaEIsS0FBQSxDQUFBLENBQUE7T0FERixNQUVPO1FBQ0wsVUFBQSxDQUFBLElBQUEsQ0FBQSxxQkFBQSxDQUFBLENBQUE7T0FDRDtNQUNELElBQUEsQ0FBQSxFQUFBLENBQUEsU0FBQSxHQUFvQixVQUFBLENBQUEsSUFBQSxDQUFwQixHQUFvQixDQUFwQixDQUFBO01BQ0EsSUFBQSxDQUFBLE9BQUEsR0FBQSxFQUFBLENBQUE7TUFDQSxJQUFBLENBQUEsTUFBQSxHQUFBLEVBQUEsQ0FBQTtNQUNBLFNBQUEsR0FBWSxDQUFDLElBQUEsQ0FBQSxNQUFBLENBQUQsU0FBQSxJQUEwQixDQUFDLGNBQUEsQ0FBM0IsS0FBMkIsQ0FBM0IsSUFBWixLQUFBLENBQUE7TUFDQSxJQUFBLEdBQU8sS0FBQSxDQUFBLFFBQUEsRUFBQSxDQUFBLEtBQUEsQ0FBQSxFQUFBLENBQUEsQ0FBUCxPQUFPLEVBQVAsQ0FBQTtNQUNBLEtBQUssRUFBQSxHQUFBLENBQUEsRUFBUSxLQUFBLEdBQVEsSUFBQSxDQUFyQixNQUFBLEVBQWtDLEVBQUEsR0FBbEMsS0FBQSxFQUFBLEVBQUEsRUFBQSxFQUFvRDtRQUNsRCxLQUFBLEdBQVEsSUFBQSxDQUFSLEVBQVEsQ0FBUixDQUFBO1FBQ0EsSUFBSSxLQUFBLEtBQUosR0FBQSxFQUFtQjtVQUNqQixTQUFBLEdBQUEsSUFBQSxDQUFBO1NBQ0Q7UUFDRCxJQUFBLENBQUEsUUFBQSxDQUFBLEtBQUEsRUFBQSxTQUFBLENBQUEsQ0FBQTtPQUNEO01BQ0QsT0FBTyxJQUFBLENBQVAsc0JBQU8sRUFBUCxDQUFBO0tBOUNGLENBQUE7O0lBaURBLFFBQUEsQ0FBQSxTQUFBLENBQUEsTUFBQSxHQUE0QixVQUFBLFFBQUEsRUFBbUI7TUFDN0MsSUFBQSxJQUFBO1VBQ0UsS0FBQSxHQURGLElBQUEsQ0FBQTtNQUVBLFFBQUEsR0FBVyxJQUFBLENBQUEsVUFBQSxDQUFYLFFBQVcsQ0FBWCxDQUFBO01BQ0EsSUFBSSxFQUFFLElBQUEsR0FBTyxRQUFBLEdBQVcsSUFBQSxDQUF4QixLQUFJLENBQUosRUFBcUM7UUFDbkMsT0FBQTtPQUNEO01BQ0QsV0FBQSxDQUFZLElBQUEsQ0FBWixFQUFBLEVBQUEsa0VBQUEsQ0FBQSxDQUFBO01BQ0EsSUFBSSxJQUFBLEdBQUosQ0FBQSxFQUFjO1FBQ1osUUFBQSxDQUFTLElBQUEsQ0FBVCxFQUFBLEVBQUEsdUJBQUEsQ0FBQSxDQUFBO09BREYsTUFFTztRQUNMLFFBQUEsQ0FBUyxJQUFBLENBQVQsRUFBQSxFQUFBLHlCQUFBLENBQUEsQ0FBQTtPQUNEO01BQ0QsSUFBQSxDQUFBLHFCQUFBLEVBQUEsQ0FBQTtNQUNBLElBQUEsQ0FBQSxPQUFBLENBQUEsUUFBQSxDQUFBLENBQUE7TUFDQSxJQUFBLENBQUEsc0JBQUEsRUFBQSxDQUFBO01BQ0EsVUFBQSxDQUFXLFlBQVc7UUFDcEIsS0FBQSxDQUFBLEVBQUEsQ0FBQSxZQUFBLENBQUE7UUFDQSxPQUFPLFFBQUEsQ0FBUyxLQUFBLENBQVQsRUFBQSxFQUFQLG9CQUFPLENBQVAsQ0FBQTtPQUZGLEVBQUEsQ0FBQSxDQUFBLENBQUE7TUFJQSxPQUFPLElBQUEsQ0FBQSxLQUFBLEdBQVAsUUFBQSxDQUFBO0tBcEJGLENBQUE7O0lBdUJBLFFBQUEsQ0FBQSxTQUFBLENBQUEsV0FBQSxHQUFpQyxZQUFXO01BQzFDLE9BQU8sY0FBQSxDQUFQLFVBQU8sQ0FBUCxDQUFBO0tBREYsQ0FBQTs7SUFJQSxRQUFBLENBQUEsU0FBQSxDQUFBLFdBQUEsR0FBaUMsVUFBQSxLQUFBLEVBQUEsTUFBQSxFQUF3QjtNQUN2RCxJQUFJLE1BQUEsSUFBSixJQUFBLEVBQW9CO1FBQ2xCLE9BQU8sSUFBQSxDQUFBLE1BQUEsQ0FBQSxZQUFBLENBQUEsS0FBQSxFQUFQLE1BQU8sQ0FBUCxDQUFBO09BREYsTUFFTyxJQUFJLENBQUMsSUFBQSxDQUFBLE1BQUEsQ0FBQSxRQUFBLENBQUwsTUFBQSxFQUFrQztRQUN2QyxPQUFPLElBQUEsQ0FBQSxNQUFBLENBQUEsV0FBQSxDQUFQLEtBQU8sQ0FBUCxDQUFBO09BREssTUFFQTtRQUNMLE9BQU8sSUFBQSxDQUFBLE1BQUEsQ0FBQSxZQUFBLENBQUEsS0FBQSxFQUFnQyxJQUFBLENBQUEsTUFBQSxDQUFBLFFBQUEsQ0FBdkMsQ0FBdUMsQ0FBaEMsQ0FBUCxDQUFBO09BQ0Q7S0FQSCxDQUFBOztJQVVBLFFBQUEsQ0FBQSxTQUFBLENBQUEsU0FBQSxHQUErQixVQUFBLEdBQUEsRUFBQSxNQUFBLEVBQUEsWUFBQSxFQUFvQztNQUNqRSxJQUFBLE1BQUEsQ0FBQTtNQUNBLE1BQUEsR0FBUyxjQUFBLENBQVQsZ0JBQVMsQ0FBVCxDQUFBO01BQ0EsTUFBQSxDQUFBLFNBQUEsR0FBQSxHQUFBLENBQUE7TUFDQSxJQUFBLFlBQUEsRUFBa0I7UUFDaEIsUUFBQSxDQUFBLE1BQUEsRUFBQSxZQUFBLENBQUEsQ0FBQTtPQUNEO01BQ0QsT0FBTyxJQUFBLENBQUEsV0FBQSxDQUFBLE1BQUEsRUFBUCxNQUFPLENBQVAsQ0FBQTtLQVBGLENBQUE7O0lBVUEsUUFBQSxDQUFBLFNBQUEsQ0FBQSxRQUFBLEdBQThCLFVBQUEsS0FBQSxFQUFBLFNBQUEsRUFBMkI7TUFDdkQsSUFBQSxHQUFBLEVBQUEsS0FBQSxFQUFBLFFBQUEsRUFBQSxJQUFBLENBQUE7TUFDQSxJQUFJLFNBQUEsSUFBSixJQUFBLEVBQXVCO1FBQ3JCLFNBQUEsR0FBQSxJQUFBLENBQUE7T0FDRDtNQUNELElBQUksS0FBQSxLQUFKLEdBQUEsRUFBbUI7UUFDakIsT0FBTyxJQUFBLENBQUEsU0FBQSxDQUFBLEtBQUEsRUFBQSxJQUFBLEVBQVAsd0JBQU8sQ0FBUCxDQUFBO09BQ0Q7TUFDRCxJQUFJLEtBQUEsS0FBSixHQUFBLEVBQW1CO1FBQ2pCLE9BQU8sSUFBQSxDQUFBLFNBQUEsQ0FBZSxDQUFDLElBQUEsR0FBTyxJQUFBLENBQUEsTUFBQSxDQUFSLEtBQUEsS0FBQSxJQUFBLEdBQUEsSUFBQSxHQUFmLEdBQUEsRUFBQSxJQUFBLEVBQVAscUJBQU8sQ0FBUCxDQUFBO09BQ0Q7TUFDRCxJQUFBLFNBQUEsRUFBZTtRQUNiLFFBQUEsR0FBQSxLQUFBLENBQUE7UUFDQSxPQUFBLElBQUEsRUFBYTtVQUNYLElBQUksQ0FBQyxJQUFBLENBQUEsTUFBQSxDQUFBLFNBQUEsQ0FBTCxNQUFBLEVBQW1DO1lBQ2pDLElBQUEsUUFBQSxFQUFjO2NBQ1osTUFBTSxJQUFBLEtBQUEsQ0FBTixvQ0FBTSxDQUFOLENBQUE7YUFDRDtZQUNELElBQUEsQ0FBQSxXQUFBLEVBQUEsQ0FBQTtZQUNBLFFBQUEsR0FBQSxJQUFBLENBQUE7V0FDRDtVQUNELEdBQUEsR0FBTSxJQUFBLENBQUEsTUFBQSxDQUFBLFNBQUEsQ0FBc0IsSUFBQSxDQUFBLE1BQUEsQ0FBQSxTQUFBLENBQUEsTUFBQSxHQUE1QixDQUFNLENBQU4sQ0FBQTtVQUNBLElBQUEsQ0FBQSxNQUFBLENBQUEsU0FBQSxHQUF3QixJQUFBLENBQUEsTUFBQSxDQUFBLFNBQUEsQ0FBQSxTQUFBLENBQUEsQ0FBQSxFQUFtQyxJQUFBLENBQUEsTUFBQSxDQUFBLFNBQUEsQ0FBQSxNQUFBLEdBQTNELENBQXdCLENBQXhCLENBQUE7VUFDQSxJQUFJLEdBQUEsS0FBSixHQUFBLEVBQWlCO1lBQ2YsTUFBQTtXQUNEO1VBQ0QsSUFBQSxDQUFBLFNBQUEsQ0FBQSxHQUFBLENBQUEsQ0FBQTtTQUNEO09BQ0Y7TUFDRCxLQUFBLEdBQVEsSUFBQSxDQUFSLFdBQVEsRUFBUixDQUFBO01BQ0EsS0FBQSxDQUFBLGFBQUEsQ0FBQSxpQkFBQSxDQUFBLENBQUEsU0FBQSxHQUFBLEtBQUEsQ0FBQTtNQUNBLElBQUEsQ0FBQSxNQUFBLENBQUEsSUFBQSxDQUFBLEtBQUEsQ0FBQSxDQUFBO01BQ0EsT0FBTyxJQUFBLENBQUEsV0FBQSxDQUFQLEtBQU8sQ0FBUCxDQUFBO0tBaENGLENBQUE7O0lBbUNBLFFBQUEsQ0FBQSxTQUFBLENBQUEsT0FBQSxHQUE2QixVQUFBLFFBQUEsRUFBbUI7TUFDOUMsSUFBSSxDQUFBLGtCQUFBLElBQXVCLElBQUEsQ0FBQSxPQUFBLENBQUEsU0FBQSxLQUEzQixPQUFBLEVBQStEO1FBQzdELE9BQU8sSUFBQSxDQUFBLFlBQUEsQ0FBUCxRQUFPLENBQVAsQ0FBQTtPQURGLE1BRU87UUFDTCxPQUFPLElBQUEsQ0FBQSxZQUFBLENBQVAsUUFBTyxDQUFQLENBQUE7T0FDRDtLQUxILENBQUE7O0lBUUEsUUFBQSxDQUFBLFNBQUEsQ0FBQSxZQUFBLEdBQWtDLFVBQUEsUUFBQSxFQUFtQjtNQUNuRCxJQUFBLEdBQUE7VUFBQSxJQUFBO1VBQUEsSUFBQTtVQUFBLEtBQUE7VUFBQSxLQUFBO1VBQ0UsS0FBQSxHQURGLElBQUEsQ0FBQTtNQUVBLElBQUksRUFBRSxJQUFBLEdBQU8sQ0FBQSxRQUFBLEdBQVksSUFBQSxDQUF6QixLQUFJLENBQUosRUFBc0M7UUFDcEMsT0FBQTtPQUNEO01BQ0QsS0FBQSxHQUFRLElBQUEsR0FBUixHQUFBLEVBQUEsQ0FBQTtNQUNBLEdBQUEsR0FBTSxJQUFBLENBQU4sS0FBQSxDQUFBO01BQ0EsT0FBTyxDQUFDLEtBQUEsR0FBTyxTQUFBLElBQUEsR0FBVztRQUN4QixJQUFBLEtBQUEsRUFBQSxJQUFBLEVBQUEsUUFBQSxDQUFBO1FBQ0EsSUFBSyxHQUFBLEVBQUEsR0FBRCxLQUFDLEdBQWlCLEtBQUEsQ0FBQSxPQUFBLENBQXRCLFFBQUEsRUFBOEM7VUFDNUMsS0FBQSxDQUFBLEtBQUEsR0FBQSxRQUFBLENBQUE7VUFDQSxLQUFBLENBQUEsTUFBQSxFQUFBLENBQUE7VUFDQSxPQUFBLENBQVEsS0FBQSxDQUFSLEVBQUEsRUFBQSxjQUFBLENBQUEsQ0FBQTtVQUNBLE9BQUE7U0FDRDtRQUNELEtBQUEsR0FBUSxHQUFBLEVBQUEsR0FBUixJQUFBLENBQUE7UUFDQSxJQUFJLEtBQUEsR0FBSixrQkFBQSxFQUFnQztVQUM5QixJQUFBLEdBQUEsR0FBQSxFQUFBLENBQUE7VUFDQSxRQUFBLEdBQVcsS0FBQSxHQUFRLEtBQUEsQ0FBQSxPQUFBLENBQW5CLFFBQUEsQ0FBQTtVQUNBLElBQUEsR0FBTyxJQUFBLEdBQVAsUUFBQSxDQUFBO1VBQ0EsR0FBQSxJQUFBLElBQUEsQ0FBQTtVQUNBLEtBQUEsQ0FBQSxNQUFBLENBQWEsSUFBQSxDQUFBLEtBQUEsQ0FBYixHQUFhLENBQWIsQ0FBQSxDQUFBO1NBQ0Q7UUFDRCxJQUFJLHFCQUFBLElBQUosSUFBQSxFQUFtQztVQUNqQyxPQUFPLHFCQUFBLENBQVAsS0FBTyxDQUFQLENBQUE7U0FERixNQUVPO1VBQ0wsT0FBTyxVQUFBLENBQUEsS0FBQSxFQUFQLGtCQUFPLENBQVAsQ0FBQTtTQUNEO09BcEJJLEdBQVAsQ0FBQTtLQVJGLENBQUE7O0lBZ0NBLFFBQUEsQ0FBQSxTQUFBLENBQUEsYUFBQSxHQUFtQyxZQUFXO01BQzVDLElBQUEsQ0FBQSxFQUFBLEdBQUEsRUFBQSxLQUFBLEVBQUEsTUFBQSxFQUFBLEVBQUEsRUFBQSxJQUFBLENBQUE7TUFDQSxNQUFBLEdBQVMsQ0FBQSxJQUFLLFNBQUEsQ0FBTCxNQUFBLEdBQXdCLE9BQUEsQ0FBQSxJQUFBLENBQUEsU0FBQSxFQUF4QixDQUF3QixDQUF4QixHQUFULEVBQUEsQ0FBQTtNQUNBLEtBQUssQ0FBQSxHQUFJLEVBQUEsR0FBSixDQUFBLEVBQVksSUFBQSxHQUFPLE1BQUEsQ0FBeEIsTUFBQSxFQUF1QyxFQUFBLEdBQXZDLElBQUEsRUFBa0QsQ0FBQSxHQUFJLEVBQXRELEVBQUEsRUFBNEQ7UUFDMUQsS0FBQSxHQUFRLE1BQUEsQ0FBUixDQUFRLENBQVIsQ0FBQTtRQUNBLE1BQUEsQ0FBQSxDQUFBLENBQUEsR0FBWSxJQUFBLENBQUEsR0FBQSxDQUFaLEtBQVksQ0FBWixDQUFBO09BQ0Q7TUFDRCxHQUFBLEdBQU0sSUFBQSxDQUFBLEdBQUEsQ0FBQSxLQUFBLENBQUEsSUFBQSxFQUFOLE1BQU0sQ0FBTixDQUFBO01BQ0EsT0FBTyxJQUFBLENBQUEsSUFBQSxDQUFVLElBQUEsQ0FBQSxHQUFBLENBQVMsR0FBQSxHQUFULENBQUEsQ0FBQSxHQUFvQixJQUFBLENBQUEsR0FBQSxDQUFyQyxFQUFxQyxDQUE5QixDQUFQLENBQUE7S0FSRixDQUFBOztJQVdBLFFBQUEsQ0FBQSxTQUFBLENBQUEsdUJBQUEsR0FBNkMsWUFBVztNQUN0RCxJQUFBLENBQUEsRUFBQSxNQUFBLEVBQUEsS0FBQSxFQUFBLEtBQUEsRUFBQSxNQUFBLEVBQUEsRUFBQSxFQUFBLElBQUEsQ0FBQTtNQUNBLE1BQUEsR0FBUyxDQUFBLElBQUssU0FBQSxDQUFMLE1BQUEsR0FBd0IsT0FBQSxDQUFBLElBQUEsQ0FBQSxTQUFBLEVBQXhCLENBQXdCLENBQXhCLEdBQVQsRUFBQSxDQUFBO01BQ0EsTUFBQSxHQUFBLG9CQUFBLENBQUE7TUFDQSxLQUFLLENBQUEsR0FBSSxFQUFBLEdBQUosQ0FBQSxFQUFZLElBQUEsR0FBTyxNQUFBLENBQXhCLE1BQUEsRUFBdUMsRUFBQSxHQUF2QyxJQUFBLEVBQWtELENBQUEsR0FBSSxFQUF0RCxFQUFBLEVBQTREO1FBQzFELEtBQUEsR0FBUSxNQUFBLENBQVIsQ0FBUSxDQUFSLENBQUE7UUFDQSxNQUFBLENBQUEsQ0FBQSxDQUFBLEdBQVksS0FBQSxDQUFaLFFBQVksRUFBWixDQUFBO1FBQ0EsS0FBQSxHQUFRLE1BQUEsQ0FBQSxJQUFBLENBQVksTUFBQSxDQUFwQixDQUFvQixDQUFaLENBQVIsQ0FBQTtRQUNBLElBQUksS0FBQSxJQUFKLElBQUEsRUFBbUI7VUFDakIsTUFBQSxDQUFBLENBQUEsQ0FBQSxHQUFBLENBQUEsQ0FBQTtTQURGLE1BRU87VUFDTCxNQUFBLENBQUEsQ0FBQSxDQUFBLEdBQVksS0FBQSxDQUFBLENBQUEsQ0FBQSxDQUFaLE1BQUEsQ0FBQTtTQUNEO09BQ0Y7TUFDRCxPQUFPLElBQUEsQ0FBQSxHQUFBLENBQUEsS0FBQSxDQUFBLElBQUEsRUFBUCxNQUFPLENBQVAsQ0FBQTtLQWRGLENBQUE7O0lBaUJBLFFBQUEsQ0FBQSxTQUFBLENBQUEsV0FBQSxHQUFpQyxZQUFXO01BQzFDLElBQUEsQ0FBQSxNQUFBLEdBQUEsRUFBQSxDQUFBO01BQ0EsSUFBQSxDQUFBLE9BQUEsR0FBQSxFQUFBLENBQUE7TUFDQSxJQUFBLENBQUEsTUFBQSxDQUFBLFNBQUEsR0FBQSxFQUFBLENBQUE7TUFDQSxPQUFPLElBQUEsQ0FBUCxXQUFPLEVBQVAsQ0FBQTtLQUpGLENBQUE7O0lBT0EsUUFBQSxDQUFBLFNBQUEsQ0FBQSxZQUFBLEdBQWtDLFVBQUEsUUFBQSxFQUFtQjtNQUNuRCxJQUFBLE9BQUEsRUFBQSxHQUFBLEVBQUEsSUFBQSxFQUFBLFVBQUEsRUFBQSxNQUFBLEVBQUEsSUFBQSxFQUFBLEdBQUEsRUFBQSxlQUFBLEVBQUEsS0FBQSxFQUFBLE1BQUEsRUFBQSxDQUFBLEVBQUEsSUFBQSxFQUFBLENBQUEsRUFBQSxJQUFBLEVBQUEsS0FBQSxFQUFBLFFBQUEsRUFBQSxLQUFBLEVBQUEsS0FBQSxFQUFBLEVBQUEsRUFBQSxFQUFBLEVBQUEsRUFBQSxFQUFBLEVBQUEsRUFBQSxJQUFBLEVBQUEsS0FBQSxFQUFBLEtBQUEsRUFBQSxFQUFBLEVBQUEsSUFBQSxFQUFBLFFBQUEsQ0FBQTtNQUNBLFFBQUEsR0FBVyxJQUFBLENBQVgsS0FBQSxDQUFBO01BQ0EsZUFBQSxHQUFrQixJQUFBLENBQUEsdUJBQUEsQ0FBQSxRQUFBLEVBQWxCLFFBQWtCLENBQWxCLENBQUE7TUFDQSxJQUFBLGVBQUEsRUFBcUI7UUFDbkIsUUFBQSxHQUFXLFFBQUEsR0FBVyxJQUFBLENBQUEsR0FBQSxDQUFBLEVBQUEsRUFBdEIsZUFBc0IsQ0FBdEIsQ0FBQTtRQUNBLFFBQUEsR0FBVyxRQUFBLEdBQVcsSUFBQSxDQUFBLEdBQUEsQ0FBQSxFQUFBLEVBQXRCLGVBQXNCLENBQXRCLENBQUE7T0FDRDtNQUNELElBQUksRUFBRSxJQUFBLEdBQU8sUUFBQSxHQUFiLFFBQUksQ0FBSixFQUFtQztRQUNqQyxPQUFBO09BQ0Q7TUFDRCxJQUFBLENBQUEsaUJBQUEsRUFBQSxDQUFBO01BQ0EsVUFBQSxHQUFhLElBQUEsQ0FBQSxhQUFBLENBQUEsUUFBQSxFQUFiLFFBQWEsQ0FBYixDQUFBO01BQ0EsTUFBQSxHQUFBLEVBQUEsQ0FBQTtNQUNBLE9BQUEsR0FBQSxDQUFBLENBQUE7TUFDQSxLQUFLLENBQUEsR0FBSSxFQUFBLEdBQVQsQ0FBQSxFQUFpQixDQUFBLElBQUEsVUFBQSxHQUFrQixFQUFBLEdBQWxCLFVBQUEsR0FBb0MsRUFBQSxHQUFyRCxVQUFBLEVBQXNFLENBQUEsR0FBSSxDQUFBLElBQUEsVUFBQSxHQUFrQixFQUFsQixFQUFBLEdBQXlCLEVBQW5HLEVBQUEsRUFBeUc7UUFDdkcsS0FBQSxHQUFRLFFBQUEsQ0FBUyxRQUFBLEdBQVcsSUFBQSxDQUFBLEdBQUEsQ0FBQSxFQUFBLEVBQWEsVUFBQSxHQUFBLENBQUEsR0FBekMsQ0FBNEIsQ0FBcEIsQ0FBUixDQUFBO1FBQ0EsR0FBQSxHQUFNLFFBQUEsQ0FBUyxRQUFBLEdBQVcsSUFBQSxDQUFBLEdBQUEsQ0FBQSxFQUFBLEVBQWEsVUFBQSxHQUFBLENBQUEsR0FBdkMsQ0FBMEIsQ0FBcEIsQ0FBTixDQUFBO1FBQ0EsSUFBQSxHQUFPLEdBQUEsR0FBUCxLQUFBLENBQUE7UUFDQSxJQUFJLElBQUEsQ0FBQSxHQUFBLENBQUEsSUFBQSxDQUFBLEdBQWlCLElBQUEsQ0FBckIsVUFBQSxFQUFzQztVQUNwQyxNQUFBLEdBQUEsRUFBQSxDQUFBO1VBQ0EsSUFBQSxHQUFPLElBQUEsSUFBUSxJQUFBLENBQUEsVUFBQSxHQUFrQixJQUFBLENBQUEsVUFBQSxHQUFBLE9BQUEsR0FBakMsZ0JBQU8sQ0FBUCxDQUFBO1VBQ0EsR0FBQSxHQUFBLEtBQUEsQ0FBQTtVQUNBLE9BQVEsSUFBQSxHQUFBLENBQUEsSUFBWSxHQUFBLEdBQWIsR0FBQyxJQUEyQixJQUFBLEdBQUEsQ0FBQSxJQUFZLEdBQUEsR0FBL0MsR0FBQSxFQUEyRDtZQUN6RCxNQUFBLENBQUEsSUFBQSxDQUFZLElBQUEsQ0FBQSxLQUFBLENBQVosR0FBWSxDQUFaLENBQUEsQ0FBQTtZQUNBLEdBQUEsSUFBQSxJQUFBLENBQUE7V0FDRDtVQUNELElBQUksTUFBQSxDQUFPLE1BQUEsQ0FBQSxNQUFBLEdBQVAsQ0FBQSxDQUFBLEtBQUosR0FBQSxFQUF1QztZQUNyQyxNQUFBLENBQUEsSUFBQSxDQUFBLEdBQUEsQ0FBQSxDQUFBO1dBQ0Q7VUFDRCxPQUFBLEVBQUEsQ0FBQTtTQVhGLE1BWU87VUFDTCxNQUFBLEdBQVUsWUFBVztZQUNuQixRQUFBLEdBQUEsRUFBQSxDQUFBO1lBQ0EsS0FBSyxJQUFJLEVBQUEsR0FBVCxLQUFBLEVBQXFCLEtBQUEsSUFBQSxHQUFBLEdBQWUsRUFBQSxJQUFmLEdBQUEsR0FBMkIsRUFBQSxJQUFoRCxHQUFBLEVBQTJELEtBQUEsSUFBQSxHQUFBLEdBQUEsRUFBQSxFQUFBLEdBQTNELEVBQUEsRUFBQSxFQUFzRjtjQUFFLFFBQUEsQ0FBQSxJQUFBLENBQUEsRUFBQSxDQUFBLENBQUE7YUFBb0I7WUFDNUcsT0FBQSxRQUFBLENBQUE7V0FIUSxDQUFELEtBQUMsQ0FBVixJQUFVLENBQVYsQ0FBQTtTQUtEO1FBQ0QsS0FBSyxDQUFBLEdBQUksRUFBQSxHQUFKLENBQUEsRUFBWSxJQUFBLEdBQU8sTUFBQSxDQUF4QixNQUFBLEVBQXVDLEVBQUEsR0FBdkMsSUFBQSxFQUFrRCxDQUFBLEdBQUksRUFBdEQsRUFBQSxFQUE0RDtVQUMxRCxLQUFBLEdBQVEsTUFBQSxDQUFSLENBQVEsQ0FBUixDQUFBO1VBQ0EsTUFBQSxDQUFBLENBQUEsQ0FBQSxHQUFZLElBQUEsQ0FBQSxHQUFBLENBQVMsS0FBQSxHQUFyQixFQUFZLENBQVosQ0FBQTtTQUNEO1FBQ0QsTUFBQSxDQUFBLElBQUEsQ0FBQSxNQUFBLENBQUEsQ0FBQTtPQUNEO01BQ0QsSUFBQSxDQUFBLFdBQUEsRUFBQSxDQUFBO01BQ0EsSUFBQSxHQUFPLE1BQUEsQ0FBUCxPQUFPLEVBQVAsQ0FBQTtNQUNBLEtBQUssQ0FBQSxHQUFJLEVBQUEsR0FBSixDQUFBLEVBQVksS0FBQSxHQUFRLElBQUEsQ0FBekIsTUFBQSxFQUFzQyxFQUFBLEdBQXRDLEtBQUEsRUFBa0QsQ0FBQSxHQUFJLEVBQXRELEVBQUEsRUFBNEQ7UUFDMUQsTUFBQSxHQUFTLElBQUEsQ0FBVCxDQUFTLENBQVQsQ0FBQTtRQUNBLElBQUksQ0FBQyxJQUFBLENBQUEsTUFBQSxDQUFMLENBQUssQ0FBTCxFQUFxQjtVQUNuQixJQUFBLENBQUEsUUFBQSxDQUFBLEdBQUEsRUFBbUIsQ0FBQSxJQUFuQixlQUFBLENBQUEsQ0FBQTtTQUNEO1FBQ0QsSUFBSSxDQUFDLEtBQUEsR0FBUSxJQUFBLENBQVQsT0FBQSxFQUFBLENBQUEsQ0FBQSxJQUFKLElBQUEsRUFBdUM7VUFDckMsS0FBQSxDQUFBLENBQUEsQ0FBQSxHQUFXLElBQUEsQ0FBQSxNQUFBLENBQUEsQ0FBQSxDQUFBLENBQUEsYUFBQSxDQUFYLHdCQUFXLENBQVgsQ0FBQTtTQUNEO1FBQ0QsSUFBQSxDQUFBLE9BQUEsQ0FBQSxDQUFBLENBQUEsQ0FBQSxTQUFBLEdBQUEsRUFBQSxDQUFBO1FBQ0EsSUFBSSxJQUFBLEdBQUosQ0FBQSxFQUFjO1VBQ1osTUFBQSxHQUFTLE1BQUEsQ0FBVCxPQUFTLEVBQVQsQ0FBQTtTQUNEO1FBQ0QsS0FBSyxDQUFBLEdBQUksRUFBQSxHQUFKLENBQUEsRUFBWSxLQUFBLEdBQVEsTUFBQSxDQUF6QixNQUFBLEVBQXdDLEVBQUEsR0FBeEMsS0FBQSxFQUFvRCxDQUFBLEdBQUksRUFBeEQsRUFBQSxFQUE4RDtVQUM1RCxLQUFBLEdBQVEsTUFBQSxDQUFSLENBQVEsQ0FBUixDQUFBO1VBQ0EsS0FBQSxHQUFRLFFBQUEsQ0FBQSxhQUFBLENBQVIsS0FBUSxDQUFSLENBQUE7VUFDQSxLQUFBLENBQUEsU0FBQSxHQUFBLGdCQUFBLENBQUE7VUFDQSxLQUFBLENBQUEsU0FBQSxHQUFBLEtBQUEsQ0FBQTtVQUNBLElBQUEsQ0FBQSxPQUFBLENBQUEsQ0FBQSxDQUFBLENBQUEsV0FBQSxDQUFBLEtBQUEsQ0FBQSxDQUFBO1VBQ0EsSUFBSSxDQUFBLEtBQU0sTUFBQSxDQUFBLE1BQUEsR0FBVixDQUFBLEVBQTZCO1lBQzNCLFFBQUEsQ0FBQSxLQUFBLEVBQUEscUJBQUEsQ0FBQSxDQUFBO1dBQ0Q7VUFDRCxJQUFJLENBQUEsS0FBSixDQUFBLEVBQWE7WUFDWCxRQUFBLENBQUEsS0FBQSxFQUFBLHNCQUFBLENBQUEsQ0FBQTtXQUNEO1NBQ0Y7T0FDRjtNQUNELElBQUksS0FBQSxHQUFKLENBQUEsRUFBZTtRQUNiLElBQUEsQ0FBQSxRQUFBLENBQUEsR0FBQSxDQUFBLENBQUE7T0FDRDtNQUNELElBQUEsR0FBTyxJQUFBLENBQUEsTUFBQSxDQUFBLGFBQUEsQ0FBUCxzQkFBTyxDQUFQLENBQUE7TUFDQSxJQUFJLElBQUEsSUFBSixJQUFBLEVBQWtCO1FBQ2hCLElBQUEsQ0FBQSxNQUFBLENBQUEsV0FBQSxDQUFBLElBQUEsQ0FBQSxDQUFBO09BQ0Q7TUFDRCxJQUFBLGVBQUEsRUFBcUI7UUFDbkIsT0FBTyxJQUFBLENBQUEsU0FBQSxDQUFlLElBQUEsQ0FBQSxNQUFBLENBQWYsS0FBQSxFQUFrQyxJQUFBLENBQUEsTUFBQSxDQUFZLGVBQUEsR0FBOUMsQ0FBa0MsQ0FBbEMsRUFBUCxxQkFBTyxDQUFQLENBQUE7T0FDRDtLQWpGSCxDQUFBOztJQW9GQSxPQUFBLFFBQUEsQ0FBQTtHQTNiVSxFQUFaLENBQUE7O0VBK2JBLFFBQUEsQ0FBQSxPQUFBLEdBQW1CLENBQUMsSUFBQSxHQUFPLE1BQUEsQ0FBUixlQUFBLEtBQUEsSUFBQSxHQUFBLElBQUEsR0FBbkIsRUFBQSxDQUFBOztFQUVBLFVBQUEsQ0FBVyxZQUFXO0lBQ3BCLElBQUEsQ0FBQSxFQUFBLENBQUEsRUFBQSxLQUFBLEVBQUEsS0FBQSxFQUFBLFFBQUEsQ0FBQTtJQUNBLElBQUksTUFBQSxDQUFKLGVBQUEsRUFBNEI7TUFDMUIsS0FBQSxHQUFRLE1BQUEsQ0FBUixlQUFBLENBQUE7TUFDQSxRQUFBLEdBQUEsRUFBQSxDQUFBO01BQ0EsS0FBQSxDQUFBLElBQUEsS0FBQSxFQUFpQjtRQUNmLENBQUEsR0FBSSxLQUFBLENBQUosQ0FBSSxDQUFKLENBQUE7UUFDQSxRQUFBLENBQUEsSUFBQSxDQUFjLENBQUMsS0FBQSxHQUFRLFFBQUEsQ0FBVCxPQUFBLEVBQUEsQ0FBQSxDQUFBLElBQUEsSUFBQSxHQUF3QyxDQUFDLEtBQUEsR0FBUSxRQUFBLENBQVQsT0FBQSxFQUF4QyxDQUF3QyxDQUF4QyxHQUF3RSxLQUFBLENBQUEsQ0FBQSxDQUFBLEdBQXRGLENBQUEsQ0FBQSxDQUFBO09BQ0Q7TUFDRCxPQUFBLFFBQUEsQ0FBQTtLQUNEO0dBVkgsRUFBQSxDQUFBLENBQUEsQ0FBQTs7RUFhQSxRQUFBLENBQUEsSUFBQSxHQUFnQixZQUFXO0lBQ3pCLElBQUEsRUFBQSxFQUFBLFFBQUEsRUFBQSxFQUFBLEVBQUEsSUFBQSxFQUFBLEtBQUEsRUFBQSxRQUFBLENBQUE7SUFDQSxJQUFJLFFBQUEsQ0FBQSxnQkFBQSxJQUFKLElBQUEsRUFBdUM7TUFDckMsT0FBQTtLQUNEO0lBQ0QsUUFBQSxHQUFXLFFBQUEsQ0FBQSxnQkFBQSxDQUEwQixRQUFBLENBQUEsT0FBQSxDQUFBLFFBQUEsSUFBckMsV0FBVyxDQUFYLENBQUE7SUFDQSxRQUFBLEdBQUEsRUFBQSxDQUFBO0lBQ0EsS0FBSyxFQUFBLEdBQUEsQ0FBQSxFQUFRLElBQUEsR0FBTyxRQUFBLENBQXBCLE1BQUEsRUFBcUMsRUFBQSxHQUFyQyxJQUFBLEVBQUEsRUFBQSxFQUFBLEVBQXNEO01BQ3BELEVBQUEsR0FBSyxRQUFBLENBQUwsRUFBSyxDQUFMLENBQUE7TUFDQSxRQUFBLENBQUEsSUFBQSxDQUFjLEVBQUEsQ0FBQSxRQUFBLEdBQWMsSUFBQSxRQUFBLENBQWE7UUFDdkMsRUFBQSxFQUR1QyxFQUFBO1FBRXZDLEtBQUEsRUFBTyxDQUFDLEtBQUEsR0FBUSxFQUFBLENBQVQsU0FBQSxLQUFBLElBQUEsR0FBQSxLQUFBLEdBQXlDLEVBQUEsQ0FBRyxXQUFBO09BRnpCLENBQTVCLENBQUEsQ0FBQTtLQUlEO0lBQ0QsT0FBQSxRQUFBLENBQUE7R0FkRixDQUFBOztFQWlCQSxJQUFLLENBQUMsQ0FBQyxLQUFBLEdBQVEsUUFBQSxDQUFULGVBQUEsS0FBQSxJQUFBLEdBQTZDLEtBQUEsQ0FBN0MsUUFBQSxHQUE4RCxLQUEvRCxDQUFBLEtBQUQsSUFBQyxJQUFvRixRQUFBLENBQUEsaUJBQUEsSUFBekYsSUFBQSxFQUE4SDtJQUM1SCxJQUFBLEdBQU8sUUFBQSxDQUFQLGtCQUFBLENBQUE7SUFDQSxRQUFBLENBQUEsa0JBQUEsR0FBOEIsWUFBVztNQUN2QyxJQUFJLFFBQUEsQ0FBQSxVQUFBLEtBQUEsVUFBQSxJQUFzQyxRQUFBLENBQUEsT0FBQSxDQUFBLElBQUEsS0FBMUMsS0FBQSxFQUEyRTtRQUN6RSxRQUFBLENBQUEsSUFBQSxFQUFBLENBQUE7T0FDRDtNQUNELE9BQU8sSUFBQSxJQUFBLElBQUEsR0FBZSxJQUFBLENBQUEsS0FBQSxDQUFBLElBQUEsRUFBZixTQUFlLENBQWYsR0FBNkMsS0FBcEQsQ0FBQSxDQUFBO0tBSkYsQ0FBQTtHQUZGLE1BUU87SUFDTCxRQUFBLENBQUEsZ0JBQUEsQ0FBQSxrQkFBQSxFQUE4QyxZQUFXO01BQ3ZELElBQUksUUFBQSxDQUFBLE9BQUEsQ0FBQSxJQUFBLEtBQUosS0FBQSxFQUFxQztRQUNuQyxPQUFPLFFBQUEsQ0FBUCxJQUFPLEVBQVAsQ0FBQTtPQUNEO0tBSEgsRUFBQSxLQUFBLENBQUEsQ0FBQTtHQUtEOztFQUVELElBQUksT0FBQSxNQUFBLEtBQUEsVUFBQSxJQUFnQyxNQUFBLENBQXBDLEdBQUEsRUFBZ0Q7SUFDOUMsTUFBQSxDQUFPLENBQVAsUUFBTyxDQUFQLEVBQW1CLFlBQVc7TUFDNUIsT0FBQSxRQUFBLENBQUE7S0FERixDQUFBLENBQUE7R0FERixNQUlPLElBQUksQ0FBQSxPQUFBLE9BQUEsS0FBQSxXQUFBLEdBQUEsV0FBQSxHQUFBLE9BQUEsQ0FBQSxPQUFBLENBQUEsTUFBbUIsQ0FBdkIsV0FBQSxFQUFxQztJQUMxQyxNQUFBLENBQUEsT0FBQSxHQUFBLFFBQUEsQ0FBQTtHQURLLE1BRUE7SUFDTCxNQUFBLENBQUEsUUFBQSxHQUFBLFFBQUEsQ0FBQTtHQUNEO0NBcm5CSCxFQUFBLElBQUEsQ0FBQSxTQUFBLENBQUEsQ0FBQTs7Ozs7O0FDQUEsSUFBQSxZQUFBLFFBQUEscUJBQUEsQ0FBQTs7Ozs7Ozs7QUFFQSxFQUFBLFVBQUEsRUFBQSxJQUFBOztBQUVBLEVBQUUsWUFBVzs7QUFFVCxNQUFBLGNBQUEsRUFBQSxFQUFBLENBQUEsT0FBQSxFQUE4QixVQUFBLENBQUEsRUFBWTtBQUN0QyxZQUFJLE1BRGtDLENBQ3RDLENBRHNDLENBQ3ZCO0FBQ2YsWUFBSSxNQUFNLE9BRjRCLENBRXRDLENBRnNDLENBRWxCO0FBQ3BCLFlBQUksWUFBSixFQUFBOztBQUVBLFlBQUksU0FBUyxLQUFBLEtBQUEsQ0FBVyxLQUFBLE1BQUEsTUFBaUIsQ0FBQSxHQUFBLEdBQU8sQ0FBbkMsR0FBVyxDQUFYLElBQTRDLENBQXpELEdBQUE7QUFDQSxrQkFBQSxJQUFBLENBQUEsTUFBQTs7QUFFQSxVQUFBLGdCQUFBLEVBQUEsSUFBQSxDQUFBLE1BQUE7O0FBRUEsbUJBQVcsWUFBVTtBQUNqQixjQUFBLFVBQUEsRUFBQSxJQUFBO0FBREosU0FBQSxFQUFBLElBQUE7O0FBSUEsbUJBQVcsWUFBVTtBQUNqQixnQkFBSSxLQUFLLFNBQUEsYUFBQSxDQUFULFdBQVMsQ0FBVDs7QUFFQSxnQkFBSSxLQUFLLElBQUEsUUFBQSxDQUFhO0FBQ2xCLG9CQUFJO0FBRGMsYUFBYixDQUFUOztBQUlBLGVBQUEsU0FBQSxHQUFBLE1BQUE7QUFQSixTQUFBLEVBQUEsSUFBQTtBQWRKLEtBQUE7O0FBeUJBLE1BQUEsY0FBQSxFQUFBLEtBQUEsQ0FBd0IsWUFBVTtBQUM5QixVQUFBLFVBQUEsRUFBQSxJQUFBO0FBREosS0FBQTs7QUFJQSxNQUFBLGNBQUEsRUFBQSxLQUFBLENBQXdCLFlBQVU7QUFDOUIsVUFBQSxVQUFBLEVBQUEsSUFBQTtBQURKLEtBQUE7QUEvQkosQ0FBQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIihmdW5jdGlvbigpIHtcbiAgdmFyIENPVU5UX0ZSQU1FUkFURSwgQ09VTlRfTVNfUEVSX0ZSQU1FLCBESUdJVF9GT1JNQVQsIERJR0lUX0hUTUwsIERJR0lUX1NQRUVEQk9PU1QsIERVUkFUSU9OLCBGT1JNQVRfTUFSS19IVE1MLCBGT1JNQVRfUEFSU0VSLCBGUkFNRVJBVEUsIEZSQU1FU19QRVJfVkFMVUUsIE1TX1BFUl9GUkFNRSwgTXV0YXRpb25PYnNlcnZlciwgT2RvbWV0ZXIsIFJJQkJPTl9IVE1MLCBUUkFOU0lUSU9OX0VORF9FVkVOVFMsIFRSQU5TSVRJT05fU1VQUE9SVCwgVkFMVUVfSFRNTCwgYWRkQ2xhc3MsIGNyZWF0ZUZyb21IVE1MLCBmcmFjdGlvbmFsUGFydCwgbm93LCByZW1vdmVDbGFzcywgcmVxdWVzdEFuaW1hdGlvbkZyYW1lLCByb3VuZCwgdHJhbnNpdGlvbkNoZWNrU3R5bGVzLCB0cmlnZ2VyLCB0cnVuY2F0ZSwgd3JhcEpRdWVyeSwgX2pRdWVyeVdyYXBwZWQsIF9vbGQsIF9yZWYsIF9yZWYxLFxuICAgIF9fc2xpY2UgPSBbXS5zbGljZTtcblxuICBWQUxVRV9IVE1MID0gJzxzcGFuIGNsYXNzPVwib2RvbWV0ZXItdmFsdWVcIj48L3NwYW4+JztcblxuICBSSUJCT05fSFRNTCA9ICc8c3BhbiBjbGFzcz1cIm9kb21ldGVyLXJpYmJvblwiPjxzcGFuIGNsYXNzPVwib2RvbWV0ZXItcmliYm9uLWlubmVyXCI+JyArIFZBTFVFX0hUTUwgKyAnPC9zcGFuPjwvc3Bhbj4nO1xuXG4gIERJR0lUX0hUTUwgPSAnPHNwYW4gY2xhc3M9XCJvZG9tZXRlci1kaWdpdFwiPjxzcGFuIGNsYXNzPVwib2RvbWV0ZXItZGlnaXQtc3BhY2VyXCI+ODwvc3Bhbj48c3BhbiBjbGFzcz1cIm9kb21ldGVyLWRpZ2l0LWlubmVyXCI+JyArIFJJQkJPTl9IVE1MICsgJzwvc3Bhbj48L3NwYW4+JztcblxuICBGT1JNQVRfTUFSS19IVE1MID0gJzxzcGFuIGNsYXNzPVwib2RvbWV0ZXItZm9ybWF0dGluZy1tYXJrXCI+PC9zcGFuPic7XG5cbiAgRElHSVRfRk9STUFUID0gJygsZGRkKS5kZCc7XG5cbiAgRk9STUFUX1BBUlNFUiA9IC9eXFwoPyhbXildKilcXCk/KD86KC4pKGQrKSk/JC87XG5cbiAgRlJBTUVSQVRFID0gMzA7XG5cbiAgRFVSQVRJT04gPSAyMDAwO1xuXG4gIENPVU5UX0ZSQU1FUkFURSA9IDIwO1xuXG4gIEZSQU1FU19QRVJfVkFMVUUgPSAyO1xuXG4gIERJR0lUX1NQRUVEQk9PU1QgPSAuNTtcblxuICBNU19QRVJfRlJBTUUgPSAxMDAwIC8gRlJBTUVSQVRFO1xuXG4gIENPVU5UX01TX1BFUl9GUkFNRSA9IDEwMDAgLyBDT1VOVF9GUkFNRVJBVEU7XG5cbiAgVFJBTlNJVElPTl9FTkRfRVZFTlRTID0gJ3RyYW5zaXRpb25lbmQgd2Via2l0VHJhbnNpdGlvbkVuZCBvVHJhbnNpdGlvbkVuZCBvdHJhbnNpdGlvbmVuZCBNU1RyYW5zaXRpb25FbmQnO1xuXG4gIHRyYW5zaXRpb25DaGVja1N0eWxlcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpLnN0eWxlO1xuXG4gIFRSQU5TSVRJT05fU1VQUE9SVCA9ICh0cmFuc2l0aW9uQ2hlY2tTdHlsZXMudHJhbnNpdGlvbiAhPSBudWxsKSB8fCAodHJhbnNpdGlvbkNoZWNrU3R5bGVzLndlYmtpdFRyYW5zaXRpb24gIT0gbnVsbCkgfHwgKHRyYW5zaXRpb25DaGVja1N0eWxlcy5tb3pUcmFuc2l0aW9uICE9IG51bGwpIHx8ICh0cmFuc2l0aW9uQ2hlY2tTdHlsZXMub1RyYW5zaXRpb24gIT0gbnVsbCk7XG5cbiAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSB8fCB3aW5kb3cubW96UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8IHdpbmRvdy53ZWJraXRSZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHwgd2luZG93Lm1zUmVxdWVzdEFuaW1hdGlvbkZyYW1lO1xuXG4gIE11dGF0aW9uT2JzZXJ2ZXIgPSB3aW5kb3cuTXV0YXRpb25PYnNlcnZlciB8fCB3aW5kb3cuV2ViS2l0TXV0YXRpb25PYnNlcnZlciB8fCB3aW5kb3cuTW96TXV0YXRpb25PYnNlcnZlcjtcblxuICBjcmVhdGVGcm9tSFRNTCA9IGZ1bmN0aW9uKGh0bWwpIHtcbiAgICB2YXIgZWw7XG4gICAgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICBlbC5pbm5lckhUTUwgPSBodG1sO1xuICAgIHJldHVybiBlbC5jaGlsZHJlblswXTtcbiAgfTtcblxuICByZW1vdmVDbGFzcyA9IGZ1bmN0aW9uKGVsLCBuYW1lKSB7XG4gICAgcmV0dXJuIGVsLmNsYXNzTmFtZSA9IGVsLmNsYXNzTmFtZS5yZXBsYWNlKG5ldyBSZWdFeHAoXCIoXnwgKVwiICsgKG5hbWUuc3BsaXQoJyAnKS5qb2luKCd8JykpICsgXCIoIHwkKVwiLCAnZ2knKSwgJyAnKTtcbiAgfTtcblxuICBhZGRDbGFzcyA9IGZ1bmN0aW9uKGVsLCBuYW1lKSB7XG4gICAgcmVtb3ZlQ2xhc3MoZWwsIG5hbWUpO1xuICAgIHJldHVybiBlbC5jbGFzc05hbWUgKz0gXCIgXCIgKyBuYW1lO1xuICB9O1xuXG4gIHRyaWdnZXIgPSBmdW5jdGlvbihlbCwgbmFtZSkge1xuICAgIHZhciBldnQ7XG4gICAgaWYgKGRvY3VtZW50LmNyZWF0ZUV2ZW50ICE9IG51bGwpIHtcbiAgICAgIGV2dCA9IGRvY3VtZW50LmNyZWF0ZUV2ZW50KCdIVE1MRXZlbnRzJyk7XG4gICAgICBldnQuaW5pdEV2ZW50KG5hbWUsIHRydWUsIHRydWUpO1xuICAgICAgcmV0dXJuIGVsLmRpc3BhdGNoRXZlbnQoZXZ0KTtcbiAgICB9XG4gIH07XG5cbiAgbm93ID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIF9yZWYsIF9yZWYxO1xuICAgIHJldHVybiAoX3JlZiA9IChfcmVmMSA9IHdpbmRvdy5wZXJmb3JtYW5jZSkgIT0gbnVsbCA/IHR5cGVvZiBfcmVmMS5ub3cgPT09IFwiZnVuY3Rpb25cIiA/IF9yZWYxLm5vdygpIDogdm9pZCAwIDogdm9pZCAwKSAhPSBudWxsID8gX3JlZiA6ICsobmV3IERhdGUpO1xuICB9O1xuXG4gIHJvdW5kID0gZnVuY3Rpb24odmFsLCBwcmVjaXNpb24pIHtcbiAgICBpZiAocHJlY2lzaW9uID09IG51bGwpIHtcbiAgICAgIHByZWNpc2lvbiA9IDA7XG4gICAgfVxuICAgIGlmICghcHJlY2lzaW9uKSB7XG4gICAgICByZXR1cm4gTWF0aC5yb3VuZCh2YWwpO1xuICAgIH1cbiAgICB2YWwgKj0gTWF0aC5wb3coMTAsIHByZWNpc2lvbik7XG4gICAgdmFsICs9IDAuNTtcbiAgICB2YWwgPSBNYXRoLmZsb29yKHZhbCk7XG4gICAgcmV0dXJuIHZhbCAvPSBNYXRoLnBvdygxMCwgcHJlY2lzaW9uKTtcbiAgfTtcblxuICB0cnVuY2F0ZSA9IGZ1bmN0aW9uKHZhbCkge1xuICAgIGlmICh2YWwgPCAwKSB7XG4gICAgICByZXR1cm4gTWF0aC5jZWlsKHZhbCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBNYXRoLmZsb29yKHZhbCk7XG4gICAgfVxuICB9O1xuXG4gIGZyYWN0aW9uYWxQYXJ0ID0gZnVuY3Rpb24odmFsKSB7XG4gICAgcmV0dXJuIHZhbCAtIHJvdW5kKHZhbCk7XG4gIH07XG5cbiAgX2pRdWVyeVdyYXBwZWQgPSBmYWxzZTtcblxuICAod3JhcEpRdWVyeSA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBwcm9wZXJ0eSwgX2ksIF9sZW4sIF9yZWYsIF9yZXN1bHRzO1xuICAgIGlmIChfalF1ZXJ5V3JhcHBlZCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBpZiAod2luZG93LmpRdWVyeSAhPSBudWxsKSB7XG4gICAgICBfalF1ZXJ5V3JhcHBlZCA9IHRydWU7XG4gICAgICBfcmVmID0gWydodG1sJywgJ3RleHQnXTtcbiAgICAgIF9yZXN1bHRzID0gW107XG4gICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IF9yZWYubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcbiAgICAgICAgcHJvcGVydHkgPSBfcmVmW19pXTtcbiAgICAgICAgX3Jlc3VsdHMucHVzaCgoZnVuY3Rpb24ocHJvcGVydHkpIHtcbiAgICAgICAgICB2YXIgb2xkO1xuICAgICAgICAgIG9sZCA9IHdpbmRvdy5qUXVlcnkuZm5bcHJvcGVydHldO1xuICAgICAgICAgIHJldHVybiB3aW5kb3cualF1ZXJ5LmZuW3Byb3BlcnR5XSA9IGZ1bmN0aW9uKHZhbCkge1xuICAgICAgICAgICAgdmFyIF9yZWYxO1xuICAgICAgICAgICAgaWYgKCh2YWwgPT0gbnVsbCkgfHwgKCgoX3JlZjEgPSB0aGlzWzBdKSAhPSBudWxsID8gX3JlZjEub2RvbWV0ZXIgOiB2b2lkIDApID09IG51bGwpKSB7XG4gICAgICAgICAgICAgIHJldHVybiBvbGQuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0aGlzWzBdLm9kb21ldGVyLnVwZGF0ZSh2YWwpO1xuICAgICAgICAgIH07XG4gICAgICAgIH0pKHByb3BlcnR5KSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gX3Jlc3VsdHM7XG4gICAgfVxuICB9KSgpO1xuXG4gIHNldFRpbWVvdXQod3JhcEpRdWVyeSwgMCk7XG5cbiAgT2RvbWV0ZXIgPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gT2RvbWV0ZXIob3B0aW9ucykge1xuICAgICAgdmFyIGUsIGssIHByb3BlcnR5LCB2LCBfYmFzZSwgX2ksIF9sZW4sIF9yZWYsIF9yZWYxLCBfcmVmMixcbiAgICAgICAgX3RoaXMgPSB0aGlzO1xuICAgICAgdGhpcy5vcHRpb25zID0gb3B0aW9ucztcbiAgICAgIHRoaXMuZWwgPSB0aGlzLm9wdGlvbnMuZWw7XG4gICAgICBpZiAodGhpcy5lbC5vZG9tZXRlciAhPSBudWxsKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmVsLm9kb21ldGVyO1xuICAgICAgfVxuICAgICAgdGhpcy5lbC5vZG9tZXRlciA9IHRoaXM7XG4gICAgICBfcmVmID0gT2RvbWV0ZXIub3B0aW9ucztcbiAgICAgIGZvciAoayBpbiBfcmVmKSB7XG4gICAgICAgIHYgPSBfcmVmW2tdO1xuICAgICAgICBpZiAodGhpcy5vcHRpb25zW2tdID09IG51bGwpIHtcbiAgICAgICAgICB0aGlzLm9wdGlvbnNba10gPSB2O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAoKF9iYXNlID0gdGhpcy5vcHRpb25zKS5kdXJhdGlvbiA9PSBudWxsKSB7XG4gICAgICAgIF9iYXNlLmR1cmF0aW9uID0gRFVSQVRJT047XG4gICAgICB9XG4gICAgICB0aGlzLk1BWF9WQUxVRVMgPSAoKHRoaXMub3B0aW9ucy5kdXJhdGlvbiAvIE1TX1BFUl9GUkFNRSkgLyBGUkFNRVNfUEVSX1ZBTFVFKSB8IDA7XG4gICAgICB0aGlzLnJlc2V0Rm9ybWF0KCk7XG4gICAgICB0aGlzLnZhbHVlID0gdGhpcy5jbGVhblZhbHVlKChfcmVmMSA9IHRoaXMub3B0aW9ucy52YWx1ZSkgIT0gbnVsbCA/IF9yZWYxIDogJycpO1xuICAgICAgdGhpcy5yZW5kZXJJbnNpZGUoKTtcbiAgICAgIHRoaXMucmVuZGVyKCk7XG4gICAgICB0cnkge1xuICAgICAgICBfcmVmMiA9IFsnaW5uZXJIVE1MJywgJ2lubmVyVGV4dCcsICd0ZXh0Q29udGVudCddO1xuICAgICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IF9yZWYyLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XG4gICAgICAgICAgcHJvcGVydHkgPSBfcmVmMltfaV07XG4gICAgICAgICAgaWYgKHRoaXMuZWxbcHJvcGVydHldICE9IG51bGwpIHtcbiAgICAgICAgICAgIChmdW5jdGlvbihwcm9wZXJ0eSkge1xuICAgICAgICAgICAgICByZXR1cm4gT2JqZWN0LmRlZmluZVByb3BlcnR5KF90aGlzLmVsLCBwcm9wZXJ0eSwge1xuICAgICAgICAgICAgICAgIGdldDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgX3JlZjM7XG4gICAgICAgICAgICAgICAgICBpZiAocHJvcGVydHkgPT09ICdpbm5lckhUTUwnKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5pbnNpZGUub3V0ZXJIVE1MO1xuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChfcmVmMyA9IF90aGlzLmluc2lkZS5pbm5lclRleHQpICE9IG51bGwgPyBfcmVmMyA6IF90aGlzLmluc2lkZS50ZXh0Q29udGVudDtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHNldDogZnVuY3Rpb24odmFsKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gX3RoaXMudXBkYXRlKHZhbCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pKHByb3BlcnR5KTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gY2F0Y2ggKF9lcnJvcikge1xuICAgICAgICBlID0gX2Vycm9yO1xuICAgICAgICB0aGlzLndhdGNoRm9yTXV0YXRpb25zKCk7XG4gICAgICB9XG4gICAgICB0aGlzO1xuICAgIH1cblxuICAgIE9kb21ldGVyLnByb3RvdHlwZS5yZW5kZXJJbnNpZGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMuaW5zaWRlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICB0aGlzLmluc2lkZS5jbGFzc05hbWUgPSAnb2RvbWV0ZXItaW5zaWRlJztcbiAgICAgIHRoaXMuZWwuaW5uZXJIVE1MID0gJyc7XG4gICAgICByZXR1cm4gdGhpcy5lbC5hcHBlbmRDaGlsZCh0aGlzLmluc2lkZSk7XG4gICAgfTtcblxuICAgIE9kb21ldGVyLnByb3RvdHlwZS53YXRjaEZvck11dGF0aW9ucyA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGUsXG4gICAgICAgIF90aGlzID0gdGhpcztcbiAgICAgIGlmIChNdXRhdGlvbk9ic2VydmVyID09IG51bGwpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdHJ5IHtcbiAgICAgICAgaWYgKHRoaXMub2JzZXJ2ZXIgPT0gbnVsbCkge1xuICAgICAgICAgIHRoaXMub2JzZXJ2ZXIgPSBuZXcgTXV0YXRpb25PYnNlcnZlcihmdW5jdGlvbihtdXRhdGlvbnMpIHtcbiAgICAgICAgICAgIHZhciBuZXdWYWw7XG4gICAgICAgICAgICBuZXdWYWwgPSBfdGhpcy5lbC5pbm5lclRleHQ7XG4gICAgICAgICAgICBfdGhpcy5yZW5kZXJJbnNpZGUoKTtcbiAgICAgICAgICAgIF90aGlzLnJlbmRlcihfdGhpcy52YWx1ZSk7XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMudXBkYXRlKG5ld1ZhbCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy53YXRjaE11dGF0aW9ucyA9IHRydWU7XG4gICAgICAgIHJldHVybiB0aGlzLnN0YXJ0V2F0Y2hpbmdNdXRhdGlvbnMoKTtcbiAgICAgIH0gY2F0Y2ggKF9lcnJvcikge1xuICAgICAgICBlID0gX2Vycm9yO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBPZG9tZXRlci5wcm90b3R5cGUuc3RhcnRXYXRjaGluZ011dGF0aW9ucyA9IGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKHRoaXMud2F0Y2hNdXRhdGlvbnMpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMub2JzZXJ2ZXIub2JzZXJ2ZSh0aGlzLmVsLCB7XG4gICAgICAgICAgY2hpbGRMaXN0OiB0cnVlXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBPZG9tZXRlci5wcm90b3R5cGUuc3RvcFdhdGNoaW5nTXV0YXRpb25zID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgX3JlZjtcbiAgICAgIHJldHVybiAoX3JlZiA9IHRoaXMub2JzZXJ2ZXIpICE9IG51bGwgPyBfcmVmLmRpc2Nvbm5lY3QoKSA6IHZvaWQgMDtcbiAgICB9O1xuXG4gICAgT2RvbWV0ZXIucHJvdG90eXBlLmNsZWFuVmFsdWUgPSBmdW5jdGlvbih2YWwpIHtcbiAgICAgIHZhciBfcmVmO1xuICAgICAgaWYgKHR5cGVvZiB2YWwgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHZhbCA9IHZhbC5yZXBsYWNlKChfcmVmID0gdGhpcy5mb3JtYXQucmFkaXgpICE9IG51bGwgPyBfcmVmIDogJy4nLCAnPHJhZGl4PicpO1xuICAgICAgICB2YWwgPSB2YWwucmVwbGFjZSgvWy4sXS9nLCAnJyk7XG4gICAgICAgIHZhbCA9IHZhbC5yZXBsYWNlKCc8cmFkaXg+JywgJy4nKTtcbiAgICAgICAgdmFsID0gcGFyc2VGbG9hdCh2YWwsIDEwKSB8fCAwO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHJvdW5kKHZhbCwgdGhpcy5mb3JtYXQucHJlY2lzaW9uKTtcbiAgICB9O1xuXG4gICAgT2RvbWV0ZXIucHJvdG90eXBlLmJpbmRUcmFuc2l0aW9uRW5kID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgZXZlbnQsIHJlbmRlckVucXVldWVkLCBfaSwgX2xlbiwgX3JlZiwgX3Jlc3VsdHMsXG4gICAgICAgIF90aGlzID0gdGhpcztcbiAgICAgIGlmICh0aGlzLnRyYW5zaXRpb25FbmRCb3VuZCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB0aGlzLnRyYW5zaXRpb25FbmRCb3VuZCA9IHRydWU7XG4gICAgICByZW5kZXJFbnF1ZXVlZCA9IGZhbHNlO1xuICAgICAgX3JlZiA9IFRSQU5TSVRJT05fRU5EX0VWRU5UUy5zcGxpdCgnICcpO1xuICAgICAgX3Jlc3VsdHMgPSBbXTtcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gX3JlZi5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xuICAgICAgICBldmVudCA9IF9yZWZbX2ldO1xuICAgICAgICBfcmVzdWx0cy5wdXNoKHRoaXMuZWwuYWRkRXZlbnRMaXN0ZW5lcihldmVudCwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYgKHJlbmRlckVucXVldWVkKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmVuZGVyRW5xdWV1ZWQgPSB0cnVlO1xuICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBfdGhpcy5yZW5kZXIoKTtcbiAgICAgICAgICAgIHJlbmRlckVucXVldWVkID0gZmFsc2U7XG4gICAgICAgICAgICByZXR1cm4gdHJpZ2dlcihfdGhpcy5lbCwgJ29kb21ldGVyZG9uZScpO1xuICAgICAgICAgIH0sIDApO1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9LCBmYWxzZSkpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIF9yZXN1bHRzO1xuICAgIH07XG5cbiAgICBPZG9tZXRlci5wcm90b3R5cGUucmVzZXRGb3JtYXQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBmb3JtYXQsIGZyYWN0aW9uYWwsIHBhcnNlZCwgcHJlY2lzaW9uLCByYWRpeCwgcmVwZWF0aW5nLCBfcmVmLCBfcmVmMTtcbiAgICAgIGZvcm1hdCA9IChfcmVmID0gdGhpcy5vcHRpb25zLmZvcm1hdCkgIT0gbnVsbCA/IF9yZWYgOiBESUdJVF9GT1JNQVQ7XG4gICAgICBmb3JtYXQgfHwgKGZvcm1hdCA9ICdkJyk7XG4gICAgICBwYXJzZWQgPSBGT1JNQVRfUEFSU0VSLmV4ZWMoZm9ybWF0KTtcbiAgICAgIGlmICghcGFyc2VkKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIk9kb21ldGVyOiBVbnBhcnNhYmxlIGRpZ2l0IGZvcm1hdFwiKTtcbiAgICAgIH1cbiAgICAgIF9yZWYxID0gcGFyc2VkLnNsaWNlKDEsIDQpLCByZXBlYXRpbmcgPSBfcmVmMVswXSwgcmFkaXggPSBfcmVmMVsxXSwgZnJhY3Rpb25hbCA9IF9yZWYxWzJdO1xuICAgICAgcHJlY2lzaW9uID0gKGZyYWN0aW9uYWwgIT0gbnVsbCA/IGZyYWN0aW9uYWwubGVuZ3RoIDogdm9pZCAwKSB8fCAwO1xuICAgICAgcmV0dXJuIHRoaXMuZm9ybWF0ID0ge1xuICAgICAgICByZXBlYXRpbmc6IHJlcGVhdGluZyxcbiAgICAgICAgcmFkaXg6IHJhZGl4LFxuICAgICAgICBwcmVjaXNpb246IHByZWNpc2lvblxuICAgICAgfTtcbiAgICB9O1xuXG4gICAgT2RvbWV0ZXIucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICB2YXIgY2xhc3NlcywgY2xzLCBkaWdpdCwgbWF0Y2gsIG5ld0NsYXNzZXMsIHRoZW1lLCB3aG9sZVBhcnQsIF9pLCBfaiwgX2xlbiwgX2xlbjEsIF9yZWY7XG4gICAgICBpZiAodmFsdWUgPT0gbnVsbCkge1xuICAgICAgICB2YWx1ZSA9IHRoaXMudmFsdWU7XG4gICAgICB9XG4gICAgICB0aGlzLnN0b3BXYXRjaGluZ011dGF0aW9ucygpO1xuICAgICAgdGhpcy5yZXNldEZvcm1hdCgpO1xuICAgICAgdGhpcy5pbnNpZGUuaW5uZXJIVE1MID0gJyc7XG4gICAgICB0aGVtZSA9IHRoaXMub3B0aW9ucy50aGVtZTtcbiAgICAgIGNsYXNzZXMgPSB0aGlzLmVsLmNsYXNzTmFtZS5zcGxpdCgnICcpO1xuICAgICAgbmV3Q2xhc3NlcyA9IFtdO1xuICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBjbGFzc2VzLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XG4gICAgICAgIGNscyA9IGNsYXNzZXNbX2ldO1xuICAgICAgICBpZiAoIWNscy5sZW5ndGgpIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAobWF0Y2ggPSAvXm9kb21ldGVyLXRoZW1lLSguKykkLy5leGVjKGNscykpIHtcbiAgICAgICAgICB0aGVtZSA9IG1hdGNoWzFdO1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG4gICAgICAgIGlmICgvXm9kb21ldGVyKC18JCkvLnRlc3QoY2xzKSkge1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG4gICAgICAgIG5ld0NsYXNzZXMucHVzaChjbHMpO1xuICAgICAgfVxuICAgICAgbmV3Q2xhc3Nlcy5wdXNoKCdvZG9tZXRlcicpO1xuICAgICAgaWYgKCFUUkFOU0lUSU9OX1NVUFBPUlQpIHtcbiAgICAgICAgbmV3Q2xhc3Nlcy5wdXNoKCdvZG9tZXRlci1uby10cmFuc2l0aW9ucycpO1xuICAgICAgfVxuICAgICAgaWYgKHRoZW1lKSB7XG4gICAgICAgIG5ld0NsYXNzZXMucHVzaChcIm9kb21ldGVyLXRoZW1lLVwiICsgdGhlbWUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgbmV3Q2xhc3Nlcy5wdXNoKFwib2RvbWV0ZXItYXV0by10aGVtZVwiKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuZWwuY2xhc3NOYW1lID0gbmV3Q2xhc3Nlcy5qb2luKCcgJyk7XG4gICAgICB0aGlzLnJpYmJvbnMgPSB7fTtcbiAgICAgIHRoaXMuZGlnaXRzID0gW107XG4gICAgICB3aG9sZVBhcnQgPSAhdGhpcy5mb3JtYXQucHJlY2lzaW9uIHx8ICFmcmFjdGlvbmFsUGFydCh2YWx1ZSkgfHwgZmFsc2U7XG4gICAgICBfcmVmID0gdmFsdWUudG9TdHJpbmcoKS5zcGxpdCgnJykucmV2ZXJzZSgpO1xuICAgICAgZm9yIChfaiA9IDAsIF9sZW4xID0gX3JlZi5sZW5ndGg7IF9qIDwgX2xlbjE7IF9qKyspIHtcbiAgICAgICAgZGlnaXQgPSBfcmVmW19qXTtcbiAgICAgICAgaWYgKGRpZ2l0ID09PSAnLicpIHtcbiAgICAgICAgICB3aG9sZVBhcnQgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuYWRkRGlnaXQoZGlnaXQsIHdob2xlUGFydCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5zdGFydFdhdGNoaW5nTXV0YXRpb25zKCk7XG4gICAgfTtcblxuICAgIE9kb21ldGVyLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbihuZXdWYWx1ZSkge1xuICAgICAgdmFyIGRpZmYsXG4gICAgICAgIF90aGlzID0gdGhpcztcbiAgICAgIG5ld1ZhbHVlID0gdGhpcy5jbGVhblZhbHVlKG5ld1ZhbHVlKTtcbiAgICAgIGlmICghKGRpZmYgPSBuZXdWYWx1ZSAtIHRoaXMudmFsdWUpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHJlbW92ZUNsYXNzKHRoaXMuZWwsICdvZG9tZXRlci1hbmltYXRpbmctdXAgb2RvbWV0ZXItYW5pbWF0aW5nLWRvd24gb2RvbWV0ZXItYW5pbWF0aW5nJyk7XG4gICAgICBpZiAoZGlmZiA+IDApIHtcbiAgICAgICAgYWRkQ2xhc3ModGhpcy5lbCwgJ29kb21ldGVyLWFuaW1hdGluZy11cCcpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYWRkQ2xhc3ModGhpcy5lbCwgJ29kb21ldGVyLWFuaW1hdGluZy1kb3duJyk7XG4gICAgICB9XG4gICAgICB0aGlzLnN0b3BXYXRjaGluZ011dGF0aW9ucygpO1xuICAgICAgdGhpcy5hbmltYXRlKG5ld1ZhbHVlKTtcbiAgICAgIHRoaXMuc3RhcnRXYXRjaGluZ011dGF0aW9ucygpO1xuICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgX3RoaXMuZWwub2Zmc2V0SGVpZ2h0O1xuICAgICAgICByZXR1cm4gYWRkQ2xhc3MoX3RoaXMuZWwsICdvZG9tZXRlci1hbmltYXRpbmcnKTtcbiAgICAgIH0sIDApO1xuICAgICAgcmV0dXJuIHRoaXMudmFsdWUgPSBuZXdWYWx1ZTtcbiAgICB9O1xuXG4gICAgT2RvbWV0ZXIucHJvdG90eXBlLnJlbmRlckRpZ2l0ID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gY3JlYXRlRnJvbUhUTUwoRElHSVRfSFRNTCk7XG4gICAgfTtcblxuICAgIE9kb21ldGVyLnByb3RvdHlwZS5pbnNlcnREaWdpdCA9IGZ1bmN0aW9uKGRpZ2l0LCBiZWZvcmUpIHtcbiAgICAgIGlmIChiZWZvcmUgIT0gbnVsbCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnNpZGUuaW5zZXJ0QmVmb3JlKGRpZ2l0LCBiZWZvcmUpO1xuICAgICAgfSBlbHNlIGlmICghdGhpcy5pbnNpZGUuY2hpbGRyZW4ubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmluc2lkZS5hcHBlbmRDaGlsZChkaWdpdCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnNpZGUuaW5zZXJ0QmVmb3JlKGRpZ2l0LCB0aGlzLmluc2lkZS5jaGlsZHJlblswXSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIE9kb21ldGVyLnByb3RvdHlwZS5hZGRTcGFjZXIgPSBmdW5jdGlvbihjaHIsIGJlZm9yZSwgZXh0cmFDbGFzc2VzKSB7XG4gICAgICB2YXIgc3BhY2VyO1xuICAgICAgc3BhY2VyID0gY3JlYXRlRnJvbUhUTUwoRk9STUFUX01BUktfSFRNTCk7XG4gICAgICBzcGFjZXIuaW5uZXJIVE1MID0gY2hyO1xuICAgICAgaWYgKGV4dHJhQ2xhc3Nlcykge1xuICAgICAgICBhZGRDbGFzcyhzcGFjZXIsIGV4dHJhQ2xhc3Nlcyk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5pbnNlcnREaWdpdChzcGFjZXIsIGJlZm9yZSk7XG4gICAgfTtcblxuICAgIE9kb21ldGVyLnByb3RvdHlwZS5hZGREaWdpdCA9IGZ1bmN0aW9uKHZhbHVlLCByZXBlYXRpbmcpIHtcbiAgICAgIHZhciBjaHIsIGRpZ2l0LCByZXNldHRlZCwgX3JlZjtcbiAgICAgIGlmIChyZXBlYXRpbmcgPT0gbnVsbCkge1xuICAgICAgICByZXBlYXRpbmcgPSB0cnVlO1xuICAgICAgfVxuICAgICAgaWYgKHZhbHVlID09PSAnLScpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYWRkU3BhY2VyKHZhbHVlLCBudWxsLCAnb2RvbWV0ZXItbmVnYXRpb24tbWFyaycpO1xuICAgICAgfVxuICAgICAgaWYgKHZhbHVlID09PSAnLicpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYWRkU3BhY2VyKChfcmVmID0gdGhpcy5mb3JtYXQucmFkaXgpICE9IG51bGwgPyBfcmVmIDogJy4nLCBudWxsLCAnb2RvbWV0ZXItcmFkaXgtbWFyaycpO1xuICAgICAgfVxuICAgICAgaWYgKHJlcGVhdGluZykge1xuICAgICAgICByZXNldHRlZCA9IGZhbHNlO1xuICAgICAgICB3aGlsZSAodHJ1ZSkge1xuICAgICAgICAgIGlmICghdGhpcy5mb3JtYXQucmVwZWF0aW5nLmxlbmd0aCkge1xuICAgICAgICAgICAgaWYgKHJlc2V0dGVkKSB7XG4gICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkJhZCBvZG9tZXRlciBmb3JtYXQgd2l0aG91dCBkaWdpdHNcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnJlc2V0Rm9ybWF0KCk7XG4gICAgICAgICAgICByZXNldHRlZCA9IHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNociA9IHRoaXMuZm9ybWF0LnJlcGVhdGluZ1t0aGlzLmZvcm1hdC5yZXBlYXRpbmcubGVuZ3RoIC0gMV07XG4gICAgICAgICAgdGhpcy5mb3JtYXQucmVwZWF0aW5nID0gdGhpcy5mb3JtYXQucmVwZWF0aW5nLnN1YnN0cmluZygwLCB0aGlzLmZvcm1hdC5yZXBlYXRpbmcubGVuZ3RoIC0gMSk7XG4gICAgICAgICAgaWYgKGNociA9PT0gJ2QnKSB7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5hZGRTcGFjZXIoY2hyKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgZGlnaXQgPSB0aGlzLnJlbmRlckRpZ2l0KCk7XG4gICAgICBkaWdpdC5xdWVyeVNlbGVjdG9yKCcub2RvbWV0ZXItdmFsdWUnKS5pbm5lckhUTUwgPSB2YWx1ZTtcbiAgICAgIHRoaXMuZGlnaXRzLnB1c2goZGlnaXQpO1xuICAgICAgcmV0dXJuIHRoaXMuaW5zZXJ0RGlnaXQoZGlnaXQpO1xuICAgIH07XG5cbiAgICBPZG9tZXRlci5wcm90b3R5cGUuYW5pbWF0ZSA9IGZ1bmN0aW9uKG5ld1ZhbHVlKSB7XG4gICAgICBpZiAoIVRSQU5TSVRJT05fU1VQUE9SVCB8fCB0aGlzLm9wdGlvbnMuYW5pbWF0aW9uID09PSAnY291bnQnKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFuaW1hdGVDb3VudChuZXdWYWx1ZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5hbmltYXRlU2xpZGUobmV3VmFsdWUpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBPZG9tZXRlci5wcm90b3R5cGUuYW5pbWF0ZUNvdW50ID0gZnVuY3Rpb24obmV3VmFsdWUpIHtcbiAgICAgIHZhciBjdXIsIGRpZmYsIGxhc3QsIHN0YXJ0LCB0aWNrLFxuICAgICAgICBfdGhpcyA9IHRoaXM7XG4gICAgICBpZiAoIShkaWZmID0gK25ld1ZhbHVlIC0gdGhpcy52YWx1ZSkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgc3RhcnQgPSBsYXN0ID0gbm93KCk7XG4gICAgICBjdXIgPSB0aGlzLnZhbHVlO1xuICAgICAgcmV0dXJuICh0aWNrID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBkZWx0YSwgZGlzdCwgZnJhY3Rpb247XG4gICAgICAgIGlmICgobm93KCkgLSBzdGFydCkgPiBfdGhpcy5vcHRpb25zLmR1cmF0aW9uKSB7XG4gICAgICAgICAgX3RoaXMudmFsdWUgPSBuZXdWYWx1ZTtcbiAgICAgICAgICBfdGhpcy5yZW5kZXIoKTtcbiAgICAgICAgICB0cmlnZ2VyKF90aGlzLmVsLCAnb2RvbWV0ZXJkb25lJyk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGRlbHRhID0gbm93KCkgLSBsYXN0O1xuICAgICAgICBpZiAoZGVsdGEgPiBDT1VOVF9NU19QRVJfRlJBTUUpIHtcbiAgICAgICAgICBsYXN0ID0gbm93KCk7XG4gICAgICAgICAgZnJhY3Rpb24gPSBkZWx0YSAvIF90aGlzLm9wdGlvbnMuZHVyYXRpb247XG4gICAgICAgICAgZGlzdCA9IGRpZmYgKiBmcmFjdGlvbjtcbiAgICAgICAgICBjdXIgKz0gZGlzdDtcbiAgICAgICAgICBfdGhpcy5yZW5kZXIoTWF0aC5yb3VuZChjdXIpKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocmVxdWVzdEFuaW1hdGlvbkZyYW1lICE9IG51bGwpIHtcbiAgICAgICAgICByZXR1cm4gcmVxdWVzdEFuaW1hdGlvbkZyYW1lKHRpY2spO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiBzZXRUaW1lb3V0KHRpY2ssIENPVU5UX01TX1BFUl9GUkFNRSk7XG4gICAgICAgIH1cbiAgICAgIH0pKCk7XG4gICAgfTtcblxuICAgIE9kb21ldGVyLnByb3RvdHlwZS5nZXREaWdpdENvdW50ID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgaSwgbWF4LCB2YWx1ZSwgdmFsdWVzLCBfaSwgX2xlbjtcbiAgICAgIHZhbHVlcyA9IDEgPD0gYXJndW1lbnRzLmxlbmd0aCA/IF9fc2xpY2UuY2FsbChhcmd1bWVudHMsIDApIDogW107XG4gICAgICBmb3IgKGkgPSBfaSA9IDAsIF9sZW4gPSB2YWx1ZXMubGVuZ3RoOyBfaSA8IF9sZW47IGkgPSArK19pKSB7XG4gICAgICAgIHZhbHVlID0gdmFsdWVzW2ldO1xuICAgICAgICB2YWx1ZXNbaV0gPSBNYXRoLmFicyh2YWx1ZSk7XG4gICAgICB9XG4gICAgICBtYXggPSBNYXRoLm1heC5hcHBseShNYXRoLCB2YWx1ZXMpO1xuICAgICAgcmV0dXJuIE1hdGguY2VpbChNYXRoLmxvZyhtYXggKyAxKSAvIE1hdGgubG9nKDEwKSk7XG4gICAgfTtcblxuICAgIE9kb21ldGVyLnByb3RvdHlwZS5nZXRGcmFjdGlvbmFsRGlnaXRDb3VudCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGksIHBhcnNlciwgcGFydHMsIHZhbHVlLCB2YWx1ZXMsIF9pLCBfbGVuO1xuICAgICAgdmFsdWVzID0gMSA8PSBhcmd1bWVudHMubGVuZ3RoID8gX19zbGljZS5jYWxsKGFyZ3VtZW50cywgMCkgOiBbXTtcbiAgICAgIHBhcnNlciA9IC9eXFwtP1xcZCpcXC4oXFxkKj8pMCokLztcbiAgICAgIGZvciAoaSA9IF9pID0gMCwgX2xlbiA9IHZhbHVlcy5sZW5ndGg7IF9pIDwgX2xlbjsgaSA9ICsrX2kpIHtcbiAgICAgICAgdmFsdWUgPSB2YWx1ZXNbaV07XG4gICAgICAgIHZhbHVlc1tpXSA9IHZhbHVlLnRvU3RyaW5nKCk7XG4gICAgICAgIHBhcnRzID0gcGFyc2VyLmV4ZWModmFsdWVzW2ldKTtcbiAgICAgICAgaWYgKHBhcnRzID09IG51bGwpIHtcbiAgICAgICAgICB2YWx1ZXNbaV0gPSAwO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHZhbHVlc1tpXSA9IHBhcnRzWzFdLmxlbmd0aDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIE1hdGgubWF4LmFwcGx5KE1hdGgsIHZhbHVlcyk7XG4gICAgfTtcblxuICAgIE9kb21ldGVyLnByb3RvdHlwZS5yZXNldERpZ2l0cyA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5kaWdpdHMgPSBbXTtcbiAgICAgIHRoaXMucmliYm9ucyA9IFtdO1xuICAgICAgdGhpcy5pbnNpZGUuaW5uZXJIVE1MID0gJyc7XG4gICAgICByZXR1cm4gdGhpcy5yZXNldEZvcm1hdCgpO1xuICAgIH07XG5cbiAgICBPZG9tZXRlci5wcm90b3R5cGUuYW5pbWF0ZVNsaWRlID0gZnVuY3Rpb24obmV3VmFsdWUpIHtcbiAgICAgIHZhciBib29zdGVkLCBjdXIsIGRpZmYsIGRpZ2l0Q291bnQsIGRpZ2l0cywgZGlzdCwgZW5kLCBmcmFjdGlvbmFsQ291bnQsIGZyYW1lLCBmcmFtZXMsIGksIGluY3IsIGosIG1hcmssIG51bUVsLCBvbGRWYWx1ZSwgc3RhcnQsIF9iYXNlLCBfaSwgX2osIF9rLCBfbCwgX2xlbiwgX2xlbjEsIF9sZW4yLCBfbSwgX3JlZiwgX3Jlc3VsdHM7XG4gICAgICBvbGRWYWx1ZSA9IHRoaXMudmFsdWU7XG4gICAgICBmcmFjdGlvbmFsQ291bnQgPSB0aGlzLmdldEZyYWN0aW9uYWxEaWdpdENvdW50KG9sZFZhbHVlLCBuZXdWYWx1ZSk7XG4gICAgICBpZiAoZnJhY3Rpb25hbENvdW50KSB7XG4gICAgICAgIG5ld1ZhbHVlID0gbmV3VmFsdWUgKiBNYXRoLnBvdygxMCwgZnJhY3Rpb25hbENvdW50KTtcbiAgICAgICAgb2xkVmFsdWUgPSBvbGRWYWx1ZSAqIE1hdGgucG93KDEwLCBmcmFjdGlvbmFsQ291bnQpO1xuICAgICAgfVxuICAgICAgaWYgKCEoZGlmZiA9IG5ld1ZhbHVlIC0gb2xkVmFsdWUpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMuYmluZFRyYW5zaXRpb25FbmQoKTtcbiAgICAgIGRpZ2l0Q291bnQgPSB0aGlzLmdldERpZ2l0Q291bnQob2xkVmFsdWUsIG5ld1ZhbHVlKTtcbiAgICAgIGRpZ2l0cyA9IFtdO1xuICAgICAgYm9vc3RlZCA9IDA7XG4gICAgICBmb3IgKGkgPSBfaSA9IDA7IDAgPD0gZGlnaXRDb3VudCA/IF9pIDwgZGlnaXRDb3VudCA6IF9pID4gZGlnaXRDb3VudDsgaSA9IDAgPD0gZGlnaXRDb3VudCA/ICsrX2kgOiAtLV9pKSB7XG4gICAgICAgIHN0YXJ0ID0gdHJ1bmNhdGUob2xkVmFsdWUgLyBNYXRoLnBvdygxMCwgZGlnaXRDb3VudCAtIGkgLSAxKSk7XG4gICAgICAgIGVuZCA9IHRydW5jYXRlKG5ld1ZhbHVlIC8gTWF0aC5wb3coMTAsIGRpZ2l0Q291bnQgLSBpIC0gMSkpO1xuICAgICAgICBkaXN0ID0gZW5kIC0gc3RhcnQ7XG4gICAgICAgIGlmIChNYXRoLmFicyhkaXN0KSA+IHRoaXMuTUFYX1ZBTFVFUykge1xuICAgICAgICAgIGZyYW1lcyA9IFtdO1xuICAgICAgICAgIGluY3IgPSBkaXN0IC8gKHRoaXMuTUFYX1ZBTFVFUyArIHRoaXMuTUFYX1ZBTFVFUyAqIGJvb3N0ZWQgKiBESUdJVF9TUEVFREJPT1NUKTtcbiAgICAgICAgICBjdXIgPSBzdGFydDtcbiAgICAgICAgICB3aGlsZSAoKGRpc3QgPiAwICYmIGN1ciA8IGVuZCkgfHwgKGRpc3QgPCAwICYmIGN1ciA+IGVuZCkpIHtcbiAgICAgICAgICAgIGZyYW1lcy5wdXNoKE1hdGgucm91bmQoY3VyKSk7XG4gICAgICAgICAgICBjdXIgKz0gaW5jcjtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKGZyYW1lc1tmcmFtZXMubGVuZ3RoIC0gMV0gIT09IGVuZCkge1xuICAgICAgICAgICAgZnJhbWVzLnB1c2goZW5kKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgYm9vc3RlZCsrO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGZyYW1lcyA9IChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIF9yZXN1bHRzID0gW107XG4gICAgICAgICAgICBmb3IgKHZhciBfaiA9IHN0YXJ0OyBzdGFydCA8PSBlbmQgPyBfaiA8PSBlbmQgOiBfaiA+PSBlbmQ7IHN0YXJ0IDw9IGVuZCA/IF9qKysgOiBfai0tKXsgX3Jlc3VsdHMucHVzaChfaik7IH1cbiAgICAgICAgICAgIHJldHVybiBfcmVzdWx0cztcbiAgICAgICAgICB9KS5hcHBseSh0aGlzKTtcbiAgICAgICAgfVxuICAgICAgICBmb3IgKGkgPSBfayA9IDAsIF9sZW4gPSBmcmFtZXMubGVuZ3RoOyBfayA8IF9sZW47IGkgPSArK19rKSB7XG4gICAgICAgICAgZnJhbWUgPSBmcmFtZXNbaV07XG4gICAgICAgICAgZnJhbWVzW2ldID0gTWF0aC5hYnMoZnJhbWUgJSAxMCk7XG4gICAgICAgIH1cbiAgICAgICAgZGlnaXRzLnB1c2goZnJhbWVzKTtcbiAgICAgIH1cbiAgICAgIHRoaXMucmVzZXREaWdpdHMoKTtcbiAgICAgIF9yZWYgPSBkaWdpdHMucmV2ZXJzZSgpO1xuICAgICAgZm9yIChpID0gX2wgPSAwLCBfbGVuMSA9IF9yZWYubGVuZ3RoOyBfbCA8IF9sZW4xOyBpID0gKytfbCkge1xuICAgICAgICBmcmFtZXMgPSBfcmVmW2ldO1xuICAgICAgICBpZiAoIXRoaXMuZGlnaXRzW2ldKSB7XG4gICAgICAgICAgdGhpcy5hZGREaWdpdCgnICcsIGkgPj0gZnJhY3Rpb25hbENvdW50KTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoKF9iYXNlID0gdGhpcy5yaWJib25zKVtpXSA9PSBudWxsKSB7XG4gICAgICAgICAgX2Jhc2VbaV0gPSB0aGlzLmRpZ2l0c1tpXS5xdWVyeVNlbGVjdG9yKCcub2RvbWV0ZXItcmliYm9uLWlubmVyJyk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5yaWJib25zW2ldLmlubmVySFRNTCA9ICcnO1xuICAgICAgICBpZiAoZGlmZiA8IDApIHtcbiAgICAgICAgICBmcmFtZXMgPSBmcmFtZXMucmV2ZXJzZSgpO1xuICAgICAgICB9XG4gICAgICAgIGZvciAoaiA9IF9tID0gMCwgX2xlbjIgPSBmcmFtZXMubGVuZ3RoOyBfbSA8IF9sZW4yOyBqID0gKytfbSkge1xuICAgICAgICAgIGZyYW1lID0gZnJhbWVzW2pdO1xuICAgICAgICAgIG51bUVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgICAgbnVtRWwuY2xhc3NOYW1lID0gJ29kb21ldGVyLXZhbHVlJztcbiAgICAgICAgICBudW1FbC5pbm5lckhUTUwgPSBmcmFtZTtcbiAgICAgICAgICB0aGlzLnJpYmJvbnNbaV0uYXBwZW5kQ2hpbGQobnVtRWwpO1xuICAgICAgICAgIGlmIChqID09PSBmcmFtZXMubGVuZ3RoIC0gMSkge1xuICAgICAgICAgICAgYWRkQ2xhc3MobnVtRWwsICdvZG9tZXRlci1sYXN0LXZhbHVlJyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChqID09PSAwKSB7XG4gICAgICAgICAgICBhZGRDbGFzcyhudW1FbCwgJ29kb21ldGVyLWZpcnN0LXZhbHVlJyk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAoc3RhcnQgPCAwKSB7XG4gICAgICAgIHRoaXMuYWRkRGlnaXQoJy0nKTtcbiAgICAgIH1cbiAgICAgIG1hcmsgPSB0aGlzLmluc2lkZS5xdWVyeVNlbGVjdG9yKCcub2RvbWV0ZXItcmFkaXgtbWFyaycpO1xuICAgICAgaWYgKG1hcmsgIT0gbnVsbCkge1xuICAgICAgICBtYXJrLnBhcmVudC5yZW1vdmVDaGlsZChtYXJrKTtcbiAgICAgIH1cbiAgICAgIGlmIChmcmFjdGlvbmFsQ291bnQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYWRkU3BhY2VyKHRoaXMuZm9ybWF0LnJhZGl4LCB0aGlzLmRpZ2l0c1tmcmFjdGlvbmFsQ291bnQgLSAxXSwgJ29kb21ldGVyLXJhZGl4LW1hcmsnKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgcmV0dXJuIE9kb21ldGVyO1xuXG4gIH0pKCk7XG5cbiAgT2RvbWV0ZXIub3B0aW9ucyA9IChfcmVmID0gd2luZG93Lm9kb21ldGVyT3B0aW9ucykgIT0gbnVsbCA/IF9yZWYgOiB7fTtcblxuICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgIHZhciBrLCB2LCBfYmFzZSwgX3JlZjEsIF9yZXN1bHRzO1xuICAgIGlmICh3aW5kb3cub2RvbWV0ZXJPcHRpb25zKSB7XG4gICAgICBfcmVmMSA9IHdpbmRvdy5vZG9tZXRlck9wdGlvbnM7XG4gICAgICBfcmVzdWx0cyA9IFtdO1xuICAgICAgZm9yIChrIGluIF9yZWYxKSB7XG4gICAgICAgIHYgPSBfcmVmMVtrXTtcbiAgICAgICAgX3Jlc3VsdHMucHVzaCgoX2Jhc2UgPSBPZG9tZXRlci5vcHRpb25zKVtrXSAhPSBudWxsID8gKF9iYXNlID0gT2RvbWV0ZXIub3B0aW9ucylba10gOiBfYmFzZVtrXSA9IHYpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIF9yZXN1bHRzO1xuICAgIH1cbiAgfSwgMCk7XG5cbiAgT2RvbWV0ZXIuaW5pdCA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBlbCwgZWxlbWVudHMsIF9pLCBfbGVuLCBfcmVmMSwgX3Jlc3VsdHM7XG4gICAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwgPT0gbnVsbCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBlbGVtZW50cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoT2RvbWV0ZXIub3B0aW9ucy5zZWxlY3RvciB8fCAnLm9kb21ldGVyJyk7XG4gICAgX3Jlc3VsdHMgPSBbXTtcbiAgICBmb3IgKF9pID0gMCwgX2xlbiA9IGVsZW1lbnRzLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XG4gICAgICBlbCA9IGVsZW1lbnRzW19pXTtcbiAgICAgIF9yZXN1bHRzLnB1c2goZWwub2RvbWV0ZXIgPSBuZXcgT2RvbWV0ZXIoe1xuICAgICAgICBlbDogZWwsXG4gICAgICAgIHZhbHVlOiAoX3JlZjEgPSBlbC5pbm5lclRleHQpICE9IG51bGwgPyBfcmVmMSA6IGVsLnRleHRDb250ZW50XG4gICAgICB9KSk7XG4gICAgfVxuICAgIHJldHVybiBfcmVzdWx0cztcbiAgfTtcblxuICBpZiAoKCgoX3JlZjEgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpICE9IG51bGwgPyBfcmVmMS5kb1Njcm9sbCA6IHZvaWQgMCkgIT0gbnVsbCkgJiYgKGRvY3VtZW50LmNyZWF0ZUV2ZW50T2JqZWN0ICE9IG51bGwpKSB7XG4gICAgX29sZCA9IGRvY3VtZW50Lm9ucmVhZHlzdGF0ZWNoYW5nZTtcbiAgICBkb2N1bWVudC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmIChkb2N1bWVudC5yZWFkeVN0YXRlID09PSAnY29tcGxldGUnICYmIE9kb21ldGVyLm9wdGlvbnMuYXV0byAhPT0gZmFsc2UpIHtcbiAgICAgICAgT2RvbWV0ZXIuaW5pdCgpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIF9vbGQgIT0gbnVsbCA/IF9vbGQuYXBwbHkodGhpcywgYXJndW1lbnRzKSA6IHZvaWQgMDtcbiAgICB9O1xuICB9IGVsc2Uge1xuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBmdW5jdGlvbigpIHtcbiAgICAgIGlmIChPZG9tZXRlci5vcHRpb25zLmF1dG8gIT09IGZhbHNlKSB7XG4gICAgICAgIHJldHVybiBPZG9tZXRlci5pbml0KCk7XG4gICAgICB9XG4gICAgfSwgZmFsc2UpO1xuICB9XG5cbiAgaWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xuICAgIGRlZmluZShbJ2pxdWVyeSddLCBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBPZG9tZXRlcjtcbiAgICB9KTtcbiAgfSBlbHNlIGlmICh0eXBlb2YgZXhwb3J0cyA9PT0gISd1bmRlZmluZWQnKSB7XG4gICAgbW9kdWxlLmV4cG9ydHMgPSBPZG9tZXRlcjtcbiAgfSBlbHNlIHtcbiAgICB3aW5kb3cuT2RvbWV0ZXIgPSBPZG9tZXRlcjtcbiAgfVxuXG59KS5jYWxsKHRoaXMpO1xuIiwiaW1wb3J0IE11YyBmcm9tICdjb21wb25lbnRzL29kb21ldGVyJ1xyXG5cclxuJCgnLmwtbW9kYWwnKS5oaWRlKCk7XHJcblxyXG4kKGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICQoJyNidG4tbG90dGVyeScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICB2YXIgbWluID0gMDsgICAvL21pbmltdW0gbnViZXIgY2FuIHJlYWNoXHJcbiAgICAgICAgdmFyIG1heCA9IDEwOTAgKyAxOyAvL21heGltdW0gbnVtYmVyIGNhbiByZWFjaCBvbiBsZXR0ZXJ5XHJcbiAgICAgICAgdmFyIGFycmF5TGlzdCA9IFtdO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHZhciByYW5kb20gPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAoK21heCAtICttaW4pKSArICttaW47XHJcbiAgICAgICAgYXJyYXlMaXN0LnB1c2gocmFuZG9tKVxyXG4gICAgICAgIFxyXG4gICAgICAgICQoJy5udW1iZXItd2lubmVyJykuaHRtbChyYW5kb20pO1xyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICQoJy5sLW1vZGFsJykuc2hvdygpXHJcbiAgICAgICAgfSwgMTgwMClcclxuXHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcub2RvbWV0ZXInKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBvZCA9IG5ldyBPZG9tZXRlcih7XHJcbiAgICAgICAgICAgICAgICBlbDogZWxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBlbC5pbm5lckhUTUwgPSByYW5kb207XHJcbiAgICAgICAgfSwgMTAwMCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKCcuY2xvc2UtbW9kYWwnKS5jbGljayhmdW5jdGlvbigpe1xyXG4gICAgICAgICQoJy5sLW1vZGFsJykuaGlkZSgpO1xyXG4gICAgfSlcclxuICAgIFxyXG4gICAgJCgnI2Nsb3NlLW1vZGFsJykuY2xpY2soZnVuY3Rpb24oKXtcclxuICAgICAgICAkKCcubC1tb2RhbCcpLmhpZGUoKTtcclxuICAgIH0pXHJcbiAgfSk7Il19
